import 'dart:async';
import 'package:flutter/services.dart';
import 'package:iranpharma/blocs/authentication_bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:iranpharma/trackings/input_helper.dart';

import '../main_state_container.dart';

typedef void CaretMoved(Offset globalCaretPosition);
typedef void TextChanged(String text);

// Helper widget to track caret position.
class TrackingTextInput extends StatefulWidget {
  TrackingTextInput(
      {Key key, this.onCaretMoved, this.onTextChanged, this.hint, this.label, this.isObscured = false,this.icon,this.observable,this.authenticationBloc,this.name,this.inputAction,this.focus,this.targetFocus,this.keyboardType})
      : super(key: key);
  final CaretMoved onCaretMoved;
  final TextChanged onTextChanged;
  final String hint;
  final AuthenticationBloc authenticationBloc;
  final Observable<String> observable;
  final String label;
  final TextInputType keyboardType;
  final FocusNode focus;
  final FocusNode targetFocus;
  final TextInputAction inputAction;
  final bool isObscured;
  final String name;
  final bool icon;
  @override
  _TrackingTextInputState createState() => _TrackingTextInputState(this.icon,this.observable,this.authenticationBloc,this.name,this.inputAction,this.focus,this.targetFocus,this.keyboardType);
}

class _TrackingTextInputState extends State<TrackingTextInput> {
  final GlobalKey _fieldKey = GlobalKey();
  final AuthenticationBloc authenticationBloc;
  final String name;
  final FocusNode targetFocus;
  final TextInputType keyboardType;
  final TextInputAction inputAction;
  final Observable<String> observable;
  final FocusNode focus;

  @override
  _TrackingTextInputState(this.icon,this.observable,this.authenticationBloc,this.name,this.inputAction,this.focus,this.targetFocus,this.keyboardType);

  final TextEditingController _textController = TextEditingController();
  bool icon;
  Timer _debounceTimer;
  @override
  initState() {
    
    _textController.addListener(() {
      // We debounce the listener as sometimes the caret position is updated after the listener
      // this assures us we get an accurate caret position.
      if (_debounceTimer?.isActive ?? false) _debounceTimer.cancel();
      _debounceTimer = Timer(const Duration(milliseconds: 200), () {
        if (_fieldKey.currentContext != null) {
          // Find the render editable in the field.
          final RenderObject fieldBox =
          _fieldKey.currentContext.findRenderObject();
          Offset caretPosition = getCaretPosition(fieldBox);

          if (widget.onCaretMoved != null) {
            widget.onCaretMoved(caretPosition);
          }
        }
      });
      if (widget.onTextChanged != null) {
        widget.onTextChanged(_textController.text);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    
    final inheritedWidget = MainStateContainer.of(context);
    final locale = inheritedWidget.locale;
    final okIcon = (name=="username" || name=="email" ) ? Icons.accessibility_new : Icons.done;
    

    return StreamBuilder<String>(
      stream: observable,
      builder: (context, snapshot) {
          
          return Padding(
            padding: const EdgeInsets.only(bottom: 20.0),
            child: Stack(
              children : <Widget>[
              (snapshot.data!=null) ? 
                Positioned(
                  left : (locale.languageCode!='fa'&&locale.languageCode!='ar') ? null: 0.0,
                  right : (locale.languageCode!='fa'&&locale.languageCode!='ar') ? 0.0: null,
                  child:
                    (snapshot.data!="loading") ? Icon((snapshot.data=="") ? okIcon : Icons.assignment_late,color
                        :((snapshot.data=="") ? Theme.of(context).accentColor : Colors.red),)
                    :CircularProgressIndicator(),
                ):Container(),
                TextFormField(
                  textAlignVertical: TextAlignVertical.center,
                  focusNode:focus,
                  autofocus: true,
                  onFieldSubmitted: (v){
                    if(inputAction == TextInputAction.next ){
                      FocusScope.of(context).requestFocus(targetFocus);
                    }
                  },
                  style: TextStyle(
                    color: Theme.of(context).textTheme.subtitle.color,
                    textBaseline: TextBaseline.alphabetic
                  ),
                  cursorColor:Theme.of(context).accentColor,
                  textInputAction: inputAction,
                  decoration: InputDecoration(
                    hintText: widget.hint,
                    hasFloatingPlaceholder: true,
                    labelText: widget.label,
                    labelStyle: TextStyle(color:Theme.of(context).accentColor),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Theme.of(context).accentColor, width: 2.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Theme.of(context).backgroundColor, width: 2.0),
                    ),
                  ),
                  key: _fieldKey,
                  keyboardType: widget.keyboardType,
                  controller: _textController,
                  obscureText: widget.isObscured,
                  validator: (value) {
                    return value;
                  }),
              ]
            )
          );
      }
    );

  }
}