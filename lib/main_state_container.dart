import 'dart:convert';
import 'package:iranpharma/blocs/authentication_bloc.dart';
import 'package:iranpharma/blocs/database_bloc.dart';
import 'package:iranpharma/blocs/router_bloc.dart';
import 'package:iranpharma/blocs/theme_bloc.dart';
import 'package:iranpharma/localization/localization_delegate.dart';
import 'package:iranpharma/models/books.model.dart';
import 'package:iranpharma/models/users.model.dart';
import 'package:flutter/cupertino.dart';
import 'package:iranpharma/side_nav.dart';
import 'package:flutter/material.dart';
import 'package:simple_animations/simple_animations.dart';



class MainStateContainer extends StatefulWidget{
  final Widget child;
  final User me;
  final String hostName;
  final RouterBloc mainRouter;
  final SideNav sideNav; 
  final ThemeData sharedTheme;
  final String token;
  final Locale locale;
  final int chatTabNumber;
  final List<dynamic> booksList;
  final AuthenticationBloc authenticationBloc;
  final dynamic connectionsMap;
  final DemoLocalizationsDelegate localizationDelegate;

  MainStateContainer({Key key,@required this.child, this.me,this.hostName,this.sideNav,this.sharedTheme,this.authenticationBloc,this.token,this.localizationDelegate,this.locale,this.chatTabNumber,this.booksList, this.connectionsMap,this.mainRouter});

  @override
  StateContainerState createState() => StateContainerState(UniqueKey(),this.me,this.child,this.hostName,this.sideNav,this.sharedTheme,this.authenticationBloc,this.token,this.localizationDelegate,this.locale,this.chatTabNumber,this.booksList,this.connectionsMap,this.mainRouter);

  static StateContainerState of(BuildContext context){ // bayad in be home page am montaghel she
    return(context.inheritFromWidgetOfExactType(InheritedContainer) as InheritedContainer).data;
  }


}
class StateContainerState extends State<MainStateContainer> {
  User me;
  Widget child;
  AuthenticationBloc authenticationBloc;
  String hostName;
  int chatTabNumber=0;
  SideNav sideNav;
  RouterBloc mainRouter;
  ThemeData sharedTheme; 
  dynamic connectionsMap = {};
  GlobalKey<ScaffoldState> scaffoldKey;
  DatabaseBloc databaseBloc = DatabaseBloc();
  DemoLocalizationsDelegate localizationDelegate;
  List<dynamic> booksList;
  Locale locale;
  String token;

  StateContainerState(Key key,this.me,this.child,this.hostName,this.sideNav,this.sharedTheme,this.authenticationBloc,this.token,this.localizationDelegate,this.locale,this.chatTabNumber,this.booksList,this.connectionsMap,this.mainRouter);

  

  void updateUserInfo(dynamic usr) {
    var user = usr;
      if(user!=null){
        user = new User(title: usr["title"],
          email: usr["email"],
          phone: usr["phone"] ?? "",
          wallet: usr["waller"],
          role : usr["role"],
          createdAt: usr["created_at"],
          updatedAt: usr["updated_at"],
          firstname: usr["firstname"],
          favoriteMedicines: usr["favorite_medicines"],
          id : usr["id"],
          lastname : usr["lastname"]
          );
      
      }
      setState(() {
        me = user;
      });
  }

  void setChatTabNumber(pageNum) {
      var x = pageNum;
      setState(() {
        chatTabNumber = x;
      });
  }
  void setScaffoldKey(key) {
      var x = key;
      setState(() {
        scaffoldKey = x;
      });
  }
  void setToken(tk){
    var t = tk;
    setState(() {
      token=t;
    });
  }
  void updateLocalization(Locale locale) { 
    localizationDelegate = new DemoLocalizationsDelegate();
    setState((){
        localizationDelegate = localizationDelegate;
    });
    setLocale(locale);
  }
  void setLibraryList(games){
    var g = games;
    setState((){
      booksList = g;
    });
    
  }
  void setOpenChat(open){
    var x = open;
    setState(() {
      sideNav.openChat = x;
    });
  }
  Future updateHostName(String name) async {
    setState(() {
        hostName = name;
    });
    final db = await databaseBloc.getDbInstance();
    final store2 = databaseBloc.getStringStoreInstance();
    await store2.record('HOST').put(db, name);
    return;
  }
  void setLocale(Locale localex) async{
    var x = localex;
    setState(() {
        locale = x;
    });
    
    final db = await databaseBloc.getDbInstance();
    final store2 = databaseBloc.getStringStoreInstance();
    await store2.record('LANG').put(db, locale.languageCode);
    return;
  }
  void newSideNav(bool open,MultiTrackTween tween,String tweenName,bool openChat) {
  //    double pageWidth
      var sideInfo = new SideNav(open: open,
      openChat: openChat,
          pageWidth: 0.0,
          tween: tween ?? "",
          tweenName: tweenName,
          
          );
      setState(() {
        sideNav = sideInfo;
      });
  
  }
  void switchNav(){
    var open = !sideNav.open;
    setState(() {
      sideNav.open = open;
    });
  }
   void switchChat(){
    var op = !sideNav.openChat;
    setState(() {
      sideNav.openChat = op;
    });
  }
  
  void setTheme(theme){
    setState(() {
        sharedTheme = theme;
      });
    tweenSwitcher();
  }
  void setMainRouter(rt){
    setState(() {
        mainRouter = rt;
      });
    
  }
  void tweenSwitcher(){
    var sideInfo = new SideNav(open: sideNav.open,
          openChat: sideNav.openChat,
          pageWidth: 0.0,
          tween: MultiTrackTween([
        Track("color1").add(Duration(milliseconds: 500),
            ColorTween(begin: sharedTheme.accentColor, end: sharedTheme.accentColor)),
        Track("color2").add(Duration(milliseconds: 200),
            ColorTween(begin: sharedTheme.primaryColor, end: sharedTheme.accentColor)),
        Track("color3").add(Duration(milliseconds: 500),
            ColorTween(begin: sharedTheme.accentColor, end: sharedTheme.primaryColor))
      ]),
          tweenName: (sideNav.tweenName=="1") ? "2":"1" 
          );
      setState(() {
        sideNav = sideInfo;
      });
  }
  
  void removeUser() {
      setState(() {
        me = null;
      });
  }



  // scket dassan 
 

  @override
  Widget build(BuildContext context) {
    return InheritedContainer(
        child: widget.child,
        data: this,
    );
  }

}

  class InheritedContainer extends InheritedWidget{
  final Widget child;
  final StateContainerState data;

  InheritedContainer({Key key,@required this.data,@required this.child}):super(key : key, child:child);


  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }

}