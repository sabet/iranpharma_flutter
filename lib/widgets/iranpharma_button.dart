import 'package:iranpharma/localization/localization.dart';
import 'package:flutter/material.dart';

class IranpharmaButton extends StatelessWidget {
  
  final double width;
  final double height;
  final Color color;
  final Color onSplash;
  final double radius;
  final Widget child;
  final Color onHover; 
  final Function onPressed;

  const IranpharmaButton({
    Key key,
    this.width,
    this.height,
    this.onPressed, this.color, this.radius, this.child, this.onSplash, this.onHover,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      minWidth: this.width,
      height: this.height,
      child:RaisedButton(
        hoverColor: this.onHover,
        shape : new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(this.radius)),
        onPressed: this.onPressed,
        splashColor: onSplash,
        color:this.color, 
        child:this.child
        )
      );
  }
}