import 'package:iranpharma/blocs/router_bloc.dart';
import 'package:iranpharma/main_state_container.dart';

import 'package:flutter/material.dart';
class TileItem extends StatelessWidget {
  final int num;
  final RouterBloc routerBloc;
  final dynamic gameData;
  const TileItem({Key key, this.num, this.routerBloc, this.gameData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pageWidth = MediaQuery.of(context).size.width;
    final hostName = MainStateContainer.of(context).hostName;
    final hostStorage = hostName.split("/api")[0];
    return Hero(
      tag: "card$num",
      child: Card(
        color: (Theme.of(context).primaryColor==Colors.white) ? Colors.white10 : Colors.black26,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(20.0),
          ),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: Stack(
          children: <Widget>[
            SizedBox(
              height: 100.0,
              width: 100.0,
              child: Image.network(
                'https://www.gstatic.com/webp/gallery/4.webp',
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                (pageWidth < 1400.0 ) ? Expanded(
                  flex:6,
                  child:gameData.icon!=null ? Row(children:<Widget>[Expanded(child:Image.network(hostStorage + gameData.icon))]) : Container(),
                )
                :Expanded(
                  flex : (pageWidth >= 960.0) ? 3 : 2, 
                  child:gameData.icon!=null ? Row(children:<Widget>[Expanded(child:Image.network(hostStorage + gameData.icon))]) : Container(),
                ),
                Expanded(
                  flex: (pageWidth < 1400.0 ) ? 2 : 1,
                  child:Material(
                    color: (Theme.of(context).primaryColor==Colors.white) ? Colors.white:Colors.black ,
                    child: ListTile(
                      title: Text(gameData.title,style:TextStyle(fontSize:20.0,color:Theme.of(context).textTheme.subtitle.color)),
                      subtitle: Text(gameData.title),
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
              left: 0.0,
              top: 0.0,
              bottom:0.0,
              right: 0.0,
              child: Material(
                type: MaterialType.transparency,
                child: InkWell(
                  onTap: () async {
                    await Future.delayed(Duration(milliseconds: 1));
                    routerBloc.route(
                      "/game/$num"
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
