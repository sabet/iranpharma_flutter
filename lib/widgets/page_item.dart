import 'dart:io';

import 'package:after_layout/after_layout.dart';
import 'package:iranpharma/Repository/games.dart';
import 'package:iranpharma/blocs/router_bloc.dart';
import 'package:iranpharma/localization/localization.dart';
import 'package:iranpharma/main_state_container.dart';
import 'package:iranpharma/pages/games/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
class PageItem extends StatefulWidget {
  final int num;
  final RouterBloc routerBloc;
  final String name;
  
  const PageItem({Key key, this.num, this.routerBloc, this.name}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new _PageItemState(this.num,this.routerBloc,this.name);

}
class _PageItemState extends State<PageItem> with AfterLayoutMixin<PageItem> , SingleTickerProviderStateMixin{
  final int num;
  final RouterBloc routerBloc;
  final Games gamesRepository = Games();
  final String name;
  int pageNumber;
  Color clr;
  TabController _tabController;
  ScrollController _controller;
  
  _PageItemState(this.num,this.routerBloc,this.name);


  @override
  void initState() {
//    print("shit");

    _controller = ScrollController();
    _tabController = TabController(vsync: this, length: 5,initialIndex: 0);
     
    super.initState();
}
@override
  void afterFirstLayout(BuildContext context) {
    _controller.addListener(()=>_scrollListener(context));
  }
  
 void _scrollListener(BuildContext context) {
    
    if (_controller.offset > _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        clr = Theme.of(context).primaryColor;
      });
    }
  if (_controller.offset > _controller.position.minScrollExtent+150 &&
        !_controller.position.outOfRange) {
      setState(() {
        clr = Theme.of(context).primaryColor;
      });
    }
    if (_controller.offset <= _controller.position.minScrollExtent+130 &&
        !_controller.position.outOfRange) {
          if (Theme.of(context).primaryColor == Colors.white){
            setState(() {
              clr =Theme.of(context).primaryColor;
            });
          }
          else{
            setState(() {
              clr = Theme.of(context).primaryColor;
            });
            
          }
      
    }

  }

  @override
  void didUpdateWidget(oldWidget) {
    super.didUpdateWidget(oldWidget);
    
  }

  @override
  void didChangeDependencies(){
    super.didChangeDependencies();
  }

  
  @override
  void dispose() { 

    super.dispose();
  }


  @override
  Widget build(BuildContext context) {

    final inheritedWidget = MainStateContainer.of(context);
    final pageWidth = MediaQuery.of(context).size.width;
    final locale = inheritedWidget.locale;

    
    final title = Center(child:Stack(
      children: <Widget>[ Positioned(
          top:Platform.isAndroid ? 12.0 : 20.0,
          left: 10.0,
          child:Text(
            this.name,
            style: Theme.of(context).textTheme.title,
          ),
        )
      ]
    ));


   


    final innerHeaderLeading = Stack(
      children: <Widget>[
        Positioned(
        bottom : 5.0,
        width:50.0,
        height: 50.0,
        left:5.0,
        child:RotatedBox(
          quarterTurns: (locale.languageCode=='fa'||locale.languageCode=='ar') ? 2 : 0,
            child:Material(
              color:Theme.of(context).accentColor,
              child:IconButton(
                
                splashColor: Theme.of(context).accentColor,
                icon: new Icon(
                  Icons.arrow_back_ios,color:Colors.blue== Theme.of(context).primaryColor ? Colors.white:Colors.black,
                ),
                tooltip: 'Back',
                onPressed: () => _backButtonPressed(),
              ),
            )   
          )
        )
      ]
    );




    AppBar appBar = new AppBar(
      primary: false,
      centerTitle:true,
      title: title,  
      actions: <Widget>[Container()],
      leading: innerHeaderLeading,
      flexibleSpace: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Colors.black.withOpacity(0.4),
              Colors.black.withOpacity(0.1),
            ],
          ),
        ),
      ),
      backgroundColor: Colors.transparent,
    );
    final MediaQueryData mediaQuery = MediaQuery.of(context);
    final hostName = MainStateContainer.of(context).hostName;
    return Stack( 
      fit: StackFit.expand,
      overflow: Overflow.visible,
      children: <Widget>[
      Hero(
        tag: "card$num",
        child: Material(
          color: (Theme.of(context).primaryColor==Colors.white) ? Colors.blue[100] : Colors.black87,
          child:Container(
            padding: EdgeInsets.all(0.0),
              child:CustomScrollView(
              controller: _controller,
              slivers: <Widget>[ 
                MediaQuery.removePadding(
                  context: context,
                  removeTop: true,
                  child : SliverAppBar(
                    titleSpacing: 10.0,
                    floating: false,
                    pinned: true,
                    elevation: 1.0,  
                    expandedHeight: (pageWidth >= 1400.0) ? 170.0 : 150.0,
                    leading: innerHeaderLeading,
                    actions: <Widget>[clr == Theme.of(context).primaryColor ?  
                      Center(child:Container(padding: EdgeInsets.only(top: 0.0), child:SizedBox(width:50.0,height: 50.0,child:Image.network(hostName+"/Statics/images/Games/"+name.split(" ").join("")+"/Board.png",fit: BoxFit.cover))))
                      : Container()],
                    title: title,
                    brightness: Theme.of(context).brightness,
                    flexibleSpace: FlexibleSpaceBar(
                      collapseMode: CollapseMode.parallax,
                      centerTitle: true,
                      titlePadding: EdgeInsets.all(0.0),
                      title: Text("Free",
                          style: TextStyle(
                            fontSize: 12.0,
                            fontWeight: FontWeight.w900,
                            color: Theme.of(context).accentColor
                          )
                      ),
                      background:  Material(
                          color:Theme.of(context).primaryColor==Colors.white ? Colors.white : Colors.black38 ,
                          child:Image.network(hostName+"/Statics/images/Games/"+name.split(" ").join("")+"/Board.png",fit: (pageWidth < 500.0) ? BoxFit.fitWidth : BoxFit.fitHeight,)
                        ),
                    ),
                    backgroundColor: clr, 
                    centerTitle: true,                 
                  ),
                ),
                SliverFillRemaining(
                  hasScrollBody: true,
                  child: Padding(
                    padding: (MediaQuery.of(context).size.width >= 1400.0)  ? EdgeInsets.only(top:50.0,bottom:20.0,left:15.0,right:15.0) : EdgeInsets.only(top:30.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        // Material(
                        //   color: (Theme.of(context).primaryColor==Colors.white) ? Colors.black45 : Colors.white24,
                        //   child: ListTile(
                        //     title: Text("Item $num"),
                        //     subtitle: Text("This is item #$num"),
                        //   ),
                        // ),
                        Expanded(
                          child:Scaffold(
                            resizeToAvoidBottomPadding: false,
                            primary: true,
                            body: TabBarView(
                              controller: _tabController ,
                              children: [
                                // GamesMain(chatRouterBloc:this.chatRouterBloc,gamesRepository:this.gamesRepository,tabController:_tabController,name:this.name,id:this.num,),
                                // GamesJoin(chatRouterBloc:this.chatRouterBloc,gamesRepository:this.gamesRepository,name:this.name,id:this.num,),
                                // // GamesCreate(chatRouterBloc:this.chatRouterBloc,gamesRepository:this.gamesRepository,name:this.name,id:this.num,),
                                // GamesWatch(chatRouterBloc:this.chatRouterBloc,gamesRepository:this.gamesRepository,name:this.name,id:this.num,),
                                // GamesInfo(chatRouterBloc:this.chatRouterBloc,gamesRepository:this.gamesRepository,name:this.name,id:this.num,),
                                

                              ],
                            ),
                            bottomNavigationBar: bottomTabBarWidget(context),
                          )
                        ),
                      ],
                    ),
                  ),
                ),    
                ],
              ),
            )
          ),
        ),
      ]
    );
  }



  Widget bottomTabBarWidget(context){
    final locale = MainStateContainer.of(context).locale;
    final backgroundColor = (Theme.of(context).primaryColor == Colors.white) ? Colors.black12 : Colors.black12;
    return PreferredSize(
        preferredSize:Size(60.0, 60.0),
        child:Container(
          color: backgroundColor,
          constraints: BoxConstraints(maxHeight: 60.0),
          height: 60.0,
          child: new Material(
            child:TabBar(
              controller: _tabController,
              unselectedLabelColor: Theme.of(context).textTheme.subtitle.color,
              indicatorColor: Theme.of(context).accentColor,
              labelColor: Theme.of(context).accentColor,
              indicatorWeight: 3.0,
              onTap: (index){
                  setState(() {
                    pageNumber = index;
                  });
              },
              tabs: [
                Tab(icon: Icon(Icons.check_circle,size: (locale.languageCode=='fa'||locale.languageCode=='ar') ? 23.0 : 26.0,),text: (MediaQuery.of(context).size.width > 500.0) ? DemoLocalizations.of(context).trans("Main") : null),
                Tab(icon: Icon(Icons.play_circle_filled,size: (locale.languageCode=='fa'||locale.languageCode=='ar') ? 23.0 : 26.0),text:(MediaQuery.of(context).size.width > 500.0) ? DemoLocalizations.of(context).trans("Join") : null),
                Tab(icon: Icon(Icons.add_circle,size: (locale.languageCode=='fa'||locale.languageCode=='ar') ? 23.0 : 26.0),text:(MediaQuery.of(context).size.width > 500.0) ? DemoLocalizations.of(context).trans("Create") : null),
                Tab(icon: Icon(Icons.live_tv,size: (locale.languageCode=='fa'||locale.languageCode=='ar') ? 23.0 : 26.0),text:(MediaQuery.of(context).size.width > 500.0) ? DemoLocalizations.of(context).trans("Watch") : null),
                Tab(icon: Icon(Icons.more_vert,size: (locale.languageCode=='fa'||locale.languageCode=='ar') ? 23.0 : 26.0),text:(MediaQuery.of(context).size.width > 500.0) ? DemoLocalizations.of(context).trans("Info"): null),
              ],
            ),
          ),
        )
    );
  }



  void _backButtonPressed(){
    routerBloc.route("/");
  }

  

}