import 'dart:io';


import 'package:iranpharma/main_state_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

Widget InnerAppBar(leading,actions,title,context,color){
  var list;
  var paddingLeft = 15.0;
  final inheritedWidget = MainStateContainer.of(context);
  final locale = inheritedWidget.locale;
  double actionsTop = -15.0;

  if(locale.languageCode=='fa'||locale.languageCode=='ar'){
      actionsTop = -11.0;
  }
  if (leading != null) {
    paddingLeft = 0;

    
    list = [
      Positioned(
        left: 2.0,
        top:-8.0,
        child: leading,
      ),
      Positioned(
        left: 43.0,
        top: 0,
        child: title,
      ),
      (actions!=null)?Positioned(
        right: 0.0,
        top: actionsTop,
        child: actions,
      ):Container(),
    ];
  }
  else {
    list = [
      Positioned(
        left: -2.0,
        top: 0,
        child: title,
      ),
      Positioned(
        right: 0.0,
        top:actionsTop,
        child: actions,
      ),
    ];
  }

  return Material(
    color: color!=null ? color : Theme.of(context).backgroundColor,
    child: PreferredSize(
      preferredSize: Platform.isAndroid ? Size.fromHeight(30.0) :  Size.fromHeight(40.0), // here the desired height
      child: Padding(
        padding: EdgeInsets.only(
            left: paddingLeft, top: 5.0, bottom: 5.0, right: 5.0),
        child: Container(
          height: Platform.isAndroid ? 30.0 : 40.0, // here the desired height,
          child: Stack(
            children: list,
          ),
        ),
      ),
    ),
  );

}