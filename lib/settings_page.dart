import 'package:iranpharma/blocs/router_bloc.dart';
import 'package:iranpharma/localization/localization.dart';
import 'package:iranpharma/localization/localization_delegate.dart';
import 'package:iranpharma/main_state_container.dart';
import 'package:iranpharma/theme_switch_widget.dart';
import 'package:flutter/material.dart';
import 'package:iranpharma/blocs/theme_bloc.dart';


class SettingsPage extends StatefulWidget {
  final ThemeBloc themeBloc;
  final RouterBloc routerBloc;
  final ThemeData themeData;
  final DemoLocalizationsDelegate localizationDelegate;
  const SettingsPage({Key key,this.themeBloc, this.routerBloc,this.themeData,this.localizationDelegate});

  @override
  _SettingsPage createState() => new _SettingsPage(this.themeBloc,this.routerBloc,this.themeData,this.localizationDelegate);

}

class _SettingsPage extends State<SettingsPage> with AutomaticKeepAliveClientMixin<SettingsPage>{


  final ThemeBloc themeBloc;
  final RouterBloc routerBloc;
  final ThemeData themeData;
  final DemoLocalizationsDelegate localizationDelegate;
  int x = 0;
  _SettingsPage(this.themeBloc,this.routerBloc, this.themeData,this.localizationDelegate);

  @override
  void initState() {

//    routerBloc.routeObservable.listen((data){

//      print("theme route success : " + data);
//    }, onDone: () {
//      print("Task Done");
//    }, onError: (error) {
//      print("Some Error " + error);
//    });



    super.initState();
  }
  @override
  bool get wantKeepAlive => true;

  @override
  void didChangeDependencies() {
    // super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(oldWidget) {
    print(" update widget Settings : ");
    // super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final inheritedWidget = MainStateContainer.of(context);
    final locale = inheritedWidget.locale;

    final innerHeaderLeading = 
        IconButton(icon: new Icon(Icons.arrow_back_ios,color:Theme.of(context).accentColor),
      tooltip: 'Back',
      onPressed: () => _backButtonPressed(),
    );

    EdgeInsets devicePadding = MediaQuery.of(context).padding;
    final pageWidth = MediaQuery.of(context).size.width;
    final inherittedWidget = MainStateContainer.of(context);
    

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        automaticallyImplyLeading:true,
        leading:innerHeaderLeading,
        title:Text(
          DemoLocalizations.of(context).trans("SettingsHeader"),
          style: Theme.of(context).appBarTheme.textTheme.headline,
        ),
        centerTitle:true,
        actions: <Widget>[

          // new Stack(
          //   children: <Widget>[
          //     new Container(
          //       padding: EdgeInsets.only(top:20,right:10),
          //       child: (locale.languageCode=="fa") ? Text(
          //         DemoLocalizations.of(context).trans("SettingsHeader"),
          //         style: Theme.of(context).appBarTheme.textTheme.headline,
          //       ) : Container(), // pick 
          //     ),
          //   ],
          // ),
        ],
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              // RaisedButton(
              //   onPressed: () {
              //     onLocaleChange(context,Locale('es','ES'));
              //   },
              //   child: Text(
              //     'Espaniol',
              //   ),
              // ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: RaisedButton(
                  onPressed: () {
                    onLocaleChange(context,Locale('en','EN'));
                  },
                  child: Text(
                    'English',
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: RaisedButton(
                  onPressed: () {
                    onLocaleChange(context,Locale('fa','FA'));
                  },
                  child: Text(
                    'فارسی',
                  ),
                ),
              ),
              StreamBuilder<ThemeData>(
                stream: themeBloc.themeDataStream,
                initialData: Theme.of(context),
                builder: (context, snapshot) {
                  if(snapshot!=null && snapshot.hasData){
                    return Material(
                        color: Theme.of(context).backgroundColor,
                        child: SizedBox(
                            width: 62.0,
                            height: 52.0,
                            child: ThemeSwitch(themeBloc: themeBloc,
                                // sideNavBloc: sideNavBloc,
                                themeData: snapshot.data,
                                routerBloc:routerBloc,
                                activeTrackColor: snapshot.data.primaryColor,
                                activeColor: snapshot.data.accentColor)
                        ),
                      );
                    }
                    else{
                      return Container();
                    }
                  }
                )
            ],
          ),
        ),
      ),
    );
  }


  DemoTheme _buildLightTheme() {
    themeBloc.setTheme(1);
    return DemoTheme(
        'light',
        ThemeData(
          brightness: Brightness.light,
          accentColor: Color.fromRGBO(106, 50, 211, 1),
          primaryColor: Colors.white,
           primarySwatch: Colors.grey,
          scaffoldBackgroundColor: Colors.white,
          appBarTheme: AppBarTheme(
            brightness: Brightness.light,
            iconTheme: IconThemeData(
              color: Colors.deepPurple[700],
              size: 24.0,

            ),
            textTheme: new TextTheme(
              headline: new TextStyle(color: Color.fromRGBO(106, 50, 211, 1)),
              subtitle: new TextStyle(color: Colors.black),
              subhead: new TextStyle(color: Colors.black, fontSize: 12.0),
            ),
          ),
          tabBarTheme: new TabBarTheme(
  //          labelColor : Colors.white,
  //          unselectedLabelColor: Colors.white,
  //          unselectedLabelStyle: TextStyle(
  //              color: Colors.white,
  //          ),
          ),
          bottomAppBarColor: Colors.black,
          primaryColorLight: Colors.lightBlue[200],
          textTheme: new TextTheme(
            headline: new TextStyle(color: Color.fromRGBO(106, 50, 211, 1)),
            subtitle: new TextStyle(color: Colors.black),
            subhead: new TextStyle(color: Colors.white),
          ),
          primaryTextTheme: TextTheme(
              title: TextStyle(
                color: Colors.white,
              )
          ),
        ),
      );
  }

  DemoTheme _buildDarkTheme() {
    themeBloc.setTheme(2);
    return DemoTheme(
        'dark',
        ThemeData(
          brightness: Brightness.dark,
          accentColor: Colors.yellowAccent,
          primaryColor: Colors.deepPurple[900],
          bottomAppBarColor: Colors.black,
          primaryColorLight: Colors.indigoAccent,
          //          primarySwatch: Colors.grey,
          scaffoldBackgroundColor: Colors.black38,
          appBarTheme: AppBarTheme(
            brightness: Brightness.light,
            iconTheme: IconThemeData(
              color: Colors.white70,
              size: 20.0,

            ),
            textTheme: new TextTheme(
              headline: new TextStyle(color: Colors.white),
              subtitle : new TextStyle(color: Colors.white),
              subhead: new TextStyle(color: Colors.black,fontSize: 12.0),
            ),
          ),
          tabBarTheme: new TabBarTheme(
//            labelColor : Colors.white,
//            unselectedLabelStyle: TextStyle(
//                color: Colors.white,
//            ),
            unselectedLabelColor: Colors.black12,
          ),
          secondaryHeaderColor: Colors.deepPurple,
          textTheme: new TextTheme(
            headline: new TextStyle(color: Colors.white),
            subtitle : new TextStyle(color: Colors.white),
            subhead: new TextStyle(color: Colors.black),
          ),
          primaryTextTheme: TextTheme(
            title: TextStyle(
              color: Colors.white,
            ),
        ),
      ),
    );
  }

  void onLocaleChange(BuildContext context,Locale locale) {
      final inheritedWidget = MainStateContainer.of(context);
      inheritedWidget.updateLocalization(locale);
      final title = (locale.languageCode=="fa") ? "ایران فارما" : "Iranpharma"; 
      routerBloc.setAppTitle(title);
  }

  void _backButtonPressed(){
    // routerBloc.route("/");
    Navigator.of(context).pop();
  }



}










