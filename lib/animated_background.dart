import 'package:after_layout/after_layout.dart';
import 'package:iranpharma/blocs/router_bloc.dart';
import 'package:iranpharma/blocs/theme_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:simple_animations/simple_animations.dart';

class AnimatedBackground extends StatefulWidget {

  final ThemeBloc themeBloc;
  final ThemeData themeData;
  final RouterBloc routerBloc;

  AnimatedBackground({Key key,this.themeBloc, this.themeData,this.routerBloc}) : super(key: key);

  @override
  _AnimatedBackground createState() =>
      new _AnimatedBackground(this.themeBloc, this.themeData,this.routerBloc);

  void main() async {
    // See https://github.com/flutter/flutter/wiki/Desktop-shells#target-platform-override

// We use the database factory to open the database

//          await store.record('settings').put(db, {'offline': true});
  }

}

class _AnimatedBackground extends State<AnimatedBackground> with AfterLayoutMixin<AnimatedBackground> , RouteAware{
  final ThemeBloc themeBloc;
  final ThemeData themeData;
  final RouterBloc routerBloc;
  MultiTrackTween tween;
  ThemeData theme;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  _AnimatedBackground(this.themeBloc,this.themeData,this.routerBloc);

  @override
  void initState() {
    super.initState();
  }

  void main(){
    var x = themeBloc.getSelectedTheme();
    x.then((theme){
      theme = theme;
    });
  }

  @override
  void didUpdateWidget(oldWidget) {
  print("updtate widget theme selector :");
  super.didUpdateWidget(oldWidget);
  }
  @override
  void didChangeDependencies(){
  print("Change dependenncies home page : ");
    super.didChangeDependencies();
  }


  @override
  Widget build(BuildContext context) {
    if(theme==null){
      theme = Theme.of(context);
    }

  }

  @override
  void afterFirstLayout(BuildContext context) async{
    // TODO: implement afterFirstLayout
    var x = themeBloc.getSelectedTheme();
    x.then((theme){
      theme = theme;
    });
  }
}