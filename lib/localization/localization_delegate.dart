import 'dart:async';  
import 'package:iranpharma/localization/localization.dart';

import 'package:flutter/material.dart';

class DemoLocalizationsDelegate extends LocalizationsDelegate<DemoLocalizations> {  
  
  DemoLocalizationsDelegate();
  @override  
  bool isSupported(Locale locale) => ['en','es','fa'].contains(locale.languageCode);  
  
  @override  
  Future<DemoLocalizations> load(Locale locale) async {  
    DemoLocalizations localizations = new DemoLocalizations(locale);  
    await localizations.load();  
    
    print("Load ${locale.languageCode}");  
    
    return localizations;  
  }  
  
  @override  
  bool shouldReload(DemoLocalizationsDelegate old) => true;  
}