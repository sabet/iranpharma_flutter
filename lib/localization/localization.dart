import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DemoLocalizations {  
  final Locale locale;  
  DemoLocalizations(this.locale); 
  static DemoLocalizations of(BuildContext context) {  
    return Localizations.of<DemoLocalizations>(context, DemoLocalizations);  
  }  
  
  Map<String, dynamic> _sentences;  
  
  Future<bool> load() async {

    String data = await rootBundle.loadString('assets/languages/${this.locale.languageCode}.json');
    Map<String, dynamic> _result = json.decode(data);

    this._sentences = new Map();
    _result.forEach((String key, dynamic value) {
    if(!(value is String))
      this._sentences[key] = value;
    else
      this._sentences[key] = value.toString();
    });

    return true;
  }
  
  dynamic trans(String key) {  
    return this._sentences[key];  
  }  
}