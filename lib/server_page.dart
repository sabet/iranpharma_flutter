import 'dart:io';

import 'package:iranpharma/blocs/router_bloc.dart';
import 'package:iranpharma/localization/localization.dart';
import 'package:iranpharma/main_state_container.dart';
import 'package:iranpharma/widgets/inner_app_bar_widget.dart';
import 'package:flutter/material.dart';





class ServerPage extends StatefulWidget {
  final RouterBloc routerBloc;
  final String host; 

  
  const ServerPage({Key key,this.routerBloc,this.host});
  
  @override
  _ServerPage createState() => new _ServerPage(this.key,this.routerBloc,this.host);

}

class _ServerPage extends State<ServerPage>  with AutomaticKeepAliveClientMixin<ServerPage>{

  final TextEditingController controller = new TextEditingController();
  final RouterBloc routerBloc;
  String host;
  final UniqueKey key;
  
  _ServerPage(this.key,this.routerBloc,this.host);

  @override
  void initState() {

//    routerBloc.routeObservable.listen((data){

//      print("theme route success : " + data);
//    }, onDone: () {
//      print("Task Done");
//    }, onError: (error) {
//      print("Some Error " + error);
//    });



    super.initState();
  }


  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(oldWidget) {
    print(" update widget Server : ");
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final inheritedWidget = MainStateContainer.of(context);
    final locale = inheritedWidget.locale;
    final hostName = inheritedWidget.hostName;

    
    final title = Padding(
                  padding:EdgeInsets.only(left: 8.0,top:0.0),
                  child:Text(
                  "Server",
                  style: Theme.of(context).textTheme.title,
                ),);

    
    final innerHeaderLeading = 
        IconButton(icon: new Icon(Icons.arrow_back_ios,color:Theme.of(context).accentColor),
        tooltip: 'Back',
        onPressed: () => _backButtonPressed(),
        );



    return Scaffold(
      appBar: AppBar(
          
        automaticallyImplyLeading:true,
        title:Text(
          DemoLocalizations.of(context).trans("ServerHeader"),
          style: Theme.of(context).appBarTheme.textTheme.headline,
        ),
        leading: innerHeaderLeading,
        centerTitle: true,
        actions: <Widget>[
          // IconButton(
          //   icon: Icon(Icons.http),
          //   onPressed: () {
              
          //     // route(context, "/Server");
          //     routerBloc.route("/Server");
          //     // Navigator.of(context).pushNamed("/Server");
          //   },
          // ),
          // IconButton(
          //   icon: Icon(Icons.settings),
          //   onPressed: () {
              
          //     // route(context, "/Server");
          //     routerBloc.route("/Settings");
          //     // Navigator.of(context).pushNamed("/Settings");
          //   },
          // ),
          // IconButton(
          //   icon: Icon(Icons.notifications),
          //   onPressed: () {
              
          //   },
          // ),
          
        ],
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Form(
                child: TextFormField(
                  initialValue: hostName,
                  keyboardType: TextInputType.url,
                  style: TextStyle(
                    color: Theme.of(context).textTheme.subtitle.color,
                  ),
                  cursorColor:Theme.of(context).accentColor,
                  onFieldSubmitted: (text){
                    print(text);
                    // return text;
                  },
                  textInputAction: TextInputAction.done,
                  onChanged: (String text){
                    print(text);
                    setState(() {
                      host = text;
                    });
                    // return text;
                  },
                  decoration: 
                  InputDecoration(
                    hintText: "",
                    labelText: "Server Address",
                    labelStyle: Theme.of(context).textTheme.subtitle
                  ),
                  validator: (value) {
                    return value;
                  }
                ),
              ),
              
              
              RaisedButton(
                onPressed: () {

                  inheritedWidget.updateHostName(host);
                },
                child: Text(
                  'Submit',
                ),
              ),
            ],
          ),
        ),
      ),
    );
    
  }

  void _backButtonPressed(){
    // routerBloc.route("/");
    Navigator.of(context).pop();
  }


  @override
  bool get wantKeepAlive => true;



}










