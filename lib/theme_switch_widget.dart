
import 'package:iranpharma/blocs/router_bloc.dart';
import 'package:iranpharma/main_state_container.dart';
import 'package:flutter/material.dart';
import 'package:iranpharma/blocs/theme_bloc.dart';
import 'package:simple_animations/simple_animations/multi_track_tween.dart';

///
/// GET THEME SELECT PAGE BACK SHIT
///


class ThemeSwitch extends StatefulWidget {
  final ThemeBloc themeBloc;
  final Color activeTrackColor;
  final Color activeColor;
  final ThemeData themeData;

  final RouterBloc routerBloc;
  final BuildContext parentContext;
  const ThemeSwitch({Key key, this.themeBloc, this.activeTrackColor, this.activeColor, this.themeData, this.parentContext ,this.routerBloc}) : super(key: key);

  @override
  _ThemeSwitch createState() => new _ThemeSwitch(this.themeBloc,this.activeTrackColor, this.activeColor,this.themeData,this.parentContext,this.routerBloc);
  
}
class _ThemeSwitch extends State<ThemeSwitch>{
  final ThemeBloc themeBloc;
  final ThemeData themeData;
  final RouterBloc routerBloc;
  final Color activeTrackColor;
  final Color activeColor;
  final BuildContext parentContext;
  

  _ThemeSwitch(this.themeBloc, this.activeTrackColor, this.activeColor,
      this.themeData, this.parentContext, this.routerBloc);

  @override
  void initState() {
//    routerBloc.routeObservable.listen((data){

//      print("theme route success : " + data);
//    }, onDone: () {
//      print("Task Done");
//    }, onError: (error) {
//      print("Some Error " + error);
//    });


    super.initState();
  }


  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(oldWidget) {
    print(" update widget Settings : ");
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    final myInheritedWidget = MainStateContainer.of(context);
    final sideNav = myInheritedWidget.sideNav;
    return Switch(
      value: (Theme
          .of(context)
          .primaryColor == Colors.white) ? false : true,
      onChanged: (value) {
        if (!value) {
          var demoTheme = _buildLightTheme();
          themeBloc.selectedTheme.add(demoTheme);
        }
        else {
          var demoTheme = _buildDarkTheme();
          themeBloc.selectedTheme.add(demoTheme);
          
        }
        Future.delayed(Duration(milliseconds: 1000),(){
          myInheritedWidget.tweenSwitcher();
        });
      },
      activeTrackColor: Theme
          .of(context)
          .accentColor,
      activeColor: Colors.black,
      inactiveTrackColor: Theme
          .of(context)
          .accentColor,
    );
  }

  DemoTheme _buildLightTheme() {
    themeBloc.setTheme(1);
    return DemoTheme(
      'light',
      ThemeData(
          brightness: Brightness.light,
          accentColor: Colors.deepPurple[700],
          primaryColor: Colors.white,
           primarySwatch: Colors.grey,
          scaffoldBackgroundColor: Colors.white,
          appBarTheme: AppBarTheme(
            brightness: Brightness.light,
            iconTheme: IconThemeData(
              color: Colors.deepPurple[700],
              size: 24.0,

            ),
            textTheme: new TextTheme(
              headline: new TextStyle(color: Colors.deepPurple[700]),
              subtitle: new TextStyle(color: Colors.black),
              subhead: new TextStyle(color: Colors.black, fontSize: 12.0),
            ),
          ),
          tabBarTheme: new TabBarTheme(
  //          labelColor : Colors.white,
  //          unselectedLabelColor: Colors.white,
  //          unselectedLabelStyle: TextStyle(
  //              color: Colors.white,
  //          ),
          ),
          bottomAppBarColor: Colors.black,
          primaryColorLight: Colors.lightBlue[200],
          textTheme: new TextTheme(
            headline: new TextStyle(color: Colors.black),
            subtitle: new TextStyle(color: Colors.black),
            subhead: new TextStyle(color: Colors.white),
          ),
          primaryTextTheme: TextTheme(
              title: TextStyle(
                color: Colors.white,
              )
          ),
        ),
    );
  }

  DemoTheme _buildDarkTheme() {
    themeBloc.setTheme(2);
    return DemoTheme(
      'dark',
      ThemeData(
          brightness: Brightness.dark,
          accentColor: Colors.deepPurpleAccent[300],
          primaryColor: Colors.deepPurple[900],
          bottomAppBarColor: Colors.black,
          primaryColorLight: Colors.indigoAccent,
          //          primarySwatch: Colors.grey,
          scaffoldBackgroundColor: Colors.black38,
          appBarTheme: AppBarTheme(
            brightness: Brightness.dark,
            iconTheme: IconThemeData(
              color: Colors.white70,
              size: 20.0,

            ),
            textTheme: new TextTheme(
              headline: new TextStyle(color: Colors.white),
              subtitle : new TextStyle(color: Colors.white),
              subhead: new TextStyle(color: Colors.black,fontSize: 12.0),
              title: new TextStyle(color: Colors.white,fontSize: 18.0),
            ),
          ),
          tabBarTheme: new TabBarTheme(
//            labelColor : Colors.white,
//            unselectedLabelStyle: TextStyle(
//                color: Colors.white,
//            ),
            unselectedLabelColor: Colors.black12,
          ),
          secondaryHeaderColor: Colors.deepPurple,
          textTheme: new TextTheme(
            headline: new TextStyle(color: Colors.white),
            subtitle : new TextStyle(color: Colors.white),
            subhead: new TextStyle(color: Colors.black),
            title: new TextStyle(color: Colors.white,fontSize: 18.0),
          ),
          primaryTextTheme: TextTheme(
            title: TextStyle(
              color: Colors.white,
            ),
          ),
        ),
    );
  }
}










