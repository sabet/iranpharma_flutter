
import 'package:flutter/cupertino.dart';
import 'package:iranpharma/Repository/games.dart';
import 'package:iranpharma/blocs/router_bloc.dart';
import 'package:iranpharma/blocs/theme_bloc.dart';
import 'package:iranpharma/localization/localization.dart';
import 'package:iranpharma/models/books.model.dart';
import 'package:iranpharma/server_page.dart';
import 'package:iranpharma/settings_page.dart';
import 'package:iranpharma/widgets/inner_app_bar_widget.dart';
import 'package:iranpharma/widgets/page_item.dart';
import 'package:iranpharma/widgets/tile_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:after_layout/after_layout.dart';


import '../main_state_container.dart';


class GamesPage extends StatefulWidget {
  final RouterBloc routerBloc;
  final ThemeBloc themeBloc;
  final LocalizationsDelegate localizationDelegate;
  final ThemeData themeData;
  
  const GamesPage({Key key, this.routerBloc, this.themeBloc, this.localizationDelegate, this.themeData}) : super(key: key);

  @override
  _GamesPage createState() => new _GamesPage(this.routerBloc,this.themeBloc, this.localizationDelegate, this.themeData);
}
class _GamesPage extends State<GamesPage> with AfterLayoutMixin<GamesPage> , RouteAware{ // ,AutomaticKeepAliveClientMixin<GamesPage>
  final RouterBloc routerBloc;
  final ThemeBloc themeBloc;
  final LocalizationsDelegate localizationDelegate;
  final ThemeData themeData;
  StateContainerState inherittedWidget;
  String hostName;
  Games gamesRepository;
  bool loader;
  var routeObserverObject;
  BuildContext lastContext;
  int x = 0;
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = new GlobalKey<RefreshIndicatorState>();
  final navigatorKey = GlobalKey<NavigatorState>();
  String appTitle;
  _GamesPage(this.routerBloc, this.themeBloc, this.localizationDelegate, this.themeData);


  @override
  void initState() {
    appTitle = "";
    hostName = "http://116.203.231.112/api";
    gamesRepository = Games();
    loader=true;
    super.initState();
  }


  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    routeObserverObject.cancel();
    super.dispose();
  }

  @override
  void didUpdateWidget(oldWidget) {
    print(" update widget Settings : ");
    
    super.didUpdateWidget(oldWidget);
  }

  @override
  void afterFirstLayout(BuildContext context) async{
   
    final iw = MainStateContainer.of(context);
    final me = iw.me;
    // final host = iw.hostName;
    // if(me!=null && me.email!=null){

    // }
    // setState(() {
    //   hostName = host;
    //   inherittedWidget = iw;
    // });
    if((iw.booksList==null) || (iw.booksList.length==0)){
      await getLibraryList(hostName,iw);
    }
    // print(booksList [0].id);
    
    observeRouterBloc(context);
    // setState((){
    //   lastContext = context;
    // });
      
    
  }


  void observeRouterBloc(BuildContext context){
    routeObserverObject = routerBloc.routeObservable.listen((data){
      if(data!="/switchLang") {
        // route(context, data);
      }
      else{
        
        setState(() {
            appTitle=DemoLocalizations.of(context).trans("AppTitle");
        });
      }
      print("home tabs route success : " + data);
    }, onDone: () {
      print("Task Done");
    }, onError: (error) {
      print("Some Error " + error);
    });
//    routerBloc.routeObservable.doOnCancel(onCancel);
  }

Future<Null> _refresh() async {
    
  await new Future.delayed(const Duration(milliseconds:5));
  getLibraryList(null,null);
  return null;
}


getLibraryList(host,StateContainerState inherittedWid){
  var hstName = host;
  if(host==null){
    hstName = hostName;
  }
  var iw = inherittedWid;
  if(iw==null && (inherittedWid!=null)){
    inherittedWid.setLibraryList([]);
  }
  else{
    iw.setLibraryList([]);
  }
  
  gamesRepository.getLibraryList(hstName).then((response){
    if(response[0]["message"]==null){
      if(response[0]["error"]==null || !(response[0]["error"].toString().startsWith("NoSuchMethodError"))){
        List<BookModel>  booksList=[];
        for(var x=0; x < response.length ; x++){
            var book = response[x];
            var bookData = BookModel(
              color:book["color"],
              title:book["title"],
              model: book["model"],
            );
            if(response[x]["id"]!=null){
              bookData = new BookModel(
                id:book["id"],
                title:book["title"],
                model: book["model"],
                color:book["color"],
                icon:book["icon"],
                iconLibrary: book["icon_library"],
                pivot: book["pivot"],
              );
            }
            
            booksList.add(bookData);
        }
        if(iw!=null){
            iw.setLibraryList(booksList);
        }
        else{
            inherittedWid.setLibraryList(booksList);
        }
      }
      else{
        if(iw!=null){
            iw.setLibraryList(null);
        }
        else{
            inherittedWid.setLibraryList(null);
        }
        
        this.setState(() {
          loader=false;
        });
        Scaffold.of(context).showSnackBar(
          new SnackBar(
            backgroundColor: Colors.red,
          content: Text("Connection Error " + ((response[0]["status"] >= 400) ? response[0]["status"].toString() : "") ,style: TextStyle(color: Colors.white),),
        ));
      }
    }
    else{
      if(iw!=null){
            iw.setLibraryList([]);
      }
      else{
          inherittedWidget.setLibraryList([]);
      }
      Future.delayed(Duration(seconds: 3),(){
        if(loader){
          Future.delayed(Duration(milliseconds :30),(){
            iw.setLibraryList(null);
             this.setState(() {
              loader=false;
            });
            
          });
        }
      });
      Scaffold.of(context).showSnackBar(
        new SnackBar(
          backgroundColor: Colors.red,
        content: Text("Connection Error " + ((response[0]["status"] >= 400) ? response[0]["status"].toString() : "") ,style: TextStyle(color: Colors.white),),
      ));
    }
  }).whenComplete(()=>{
    print("shit")
  }).catchError((err){
      print("ERRRRR : " + err);
  }).timeout(Duration(seconds: 5));

}


  @override
  Widget build(BuildContext context) {
    
    final iw = MainStateContainer.of(context);
    // final pageWidth = MediaQuery.of(context).size.width;
    final locale = iw.locale;
    // final host = iw.hostName;
    
    // final title = Padding(
    //   padding:(locale.languageCode=='fa'||locale.languageCode=='ar') ? EdgeInsets.only(right: 5.0,top:9.5) : EdgeInsets.only(left: 8.0,top:0.0),
    //   child:Text(
    //   '',
    //   style: Theme.of(context).textTheme.title,
    // ),);


    // final innerHeaderActions = Row(children:<Widget> [
    //     (locale.languageCode=='fa'||locale.languageCode=='ar') ? 
    //       Padding(
    //         padding: (locale.languageCode=='fa'||locale.languageCode=='ar') ? EdgeInsets.only(right: 5.0,top:10.5):EdgeInsets.only(left: 10.0,top:5.0),
    //         child:Icon(Icons.present_to_all,size: 34,color:Theme.of(context).accentColor)
    //       )
    //       :Container(),
          
    //       (locale.languageCode=='fa'||locale.languageCode=='ar') ? title : Container(),
    //       //  Padding(
    //       //   padding: (locale.languageCode=='fa'||locale.languageCode=='ar') ? EdgeInsets.only(right: 20.0,top:1.0): EdgeInsets.only(right: 20.0,top:5.0),
    //       //   child:IconButton(
    //       //     icon: new Icon(Icons.keyboard,size:35),
    //       //     tooltip: 'shit',
    //       //     color: Theme.of(context).accentColor,
    //       //     focusColor: Theme.of(context).primaryColorDark,
    //       //     onPressed: () => _RightButtonPressed(context),
    //       //   ),      
    //       //  ),
    //     ]);


    // final innerHeaderLeading = RotatedBox(
    //     quarterTurns: (locale.languageCode=='fa'||locale.languageCode=='ar') ? 2 : 0,
    //     child:IconButton(icon: new Icon(Icons.replay,color:Theme.of(context).primaryColor),
    //   tooltip: 'Back',
    //   onPressed: (){
    //     _refresh();
    //   },
    // ));

    final root = drugsPage(context);
    final settingsPage = SettingsPage(key:UniqueKey(),themeBloc: themeBloc, routerBloc: routerBloc,themeData:themeData,localizationDelegate:localizationDelegate);
    final serverPage = ServerPage(routerBloc:routerBloc,);

    return Navigator(
      key: navigatorKey,
      initialRoute: "/",
      onGenerateRoute: (RouteSettings routeSettings){
        switch (routeSettings.name){
          case "/" : return BaseMaterialPageRoute(builder: (context)=> root);break;
          case "/Settings" : return MaterialPageRoute( builder: (context)=> settingsPage);break;
          case "/Server" : return MaterialPageRoute( builder: (context)=> serverPage);break;
          
          // default:{
          //   return BaseMaterialPageRoute(builder: (context)=> drugsPage(context, locale));break;
          // }
          // case 
        }
        if(routeSettings.name.startsWith("/medicine")){

          final gameId = int.parse(routeSettings.name.split("/medicine/")[1]);
          var title = "";
          


          return MaterialPageRoute(
            builder: (context) {
              return PageItem(num: gameId,name:title,routerBloc: routerBloc,);
            },
            fullscreenDialog: true,
          );
        }
        else{
          //
        }
      });
  }

  Widget drugsPage(context){
    final iw = MainStateContainer.of(context);
    final pageWidth = MediaQuery.of(context).size.width;
    final locale = iw.locale;
    return Scaffold(
      appBar: AppBar(  
        // automaticallyImplyLeading:true,
        centerTitle: true,
        title:StreamBuilder<String>(
          stream: routerBloc.appTitleObservable,
          initialData: DemoLocalizations.of(context).trans("LibraryHeader"),
          builder: (context, snapshot) {
            if(snapshot!=null && snapshot.hasData && snapshot.data=="Iranpharma" || snapshot.data=="ایران فارما"){
              return Text(
                snapshot.data,
                style: Theme.of(context).appBarTheme.textTheme.headline,
              );
            }
            else{
              return Container();
            }
          }
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.http),
            onPressed: () {
              
              // route(context, "/Server");
              // Navigator.of(context).pushNamed("/Server");
              navigatorKey.currentState.pushNamed("/Server");
              // Navigator.of(context).pushNamed("/Server");
            },
          ),
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () {
              
              // route(context, "/Server");
              // Navigator.of(context).pushNamed("/Settings");
              navigatorKey.currentState.pushNamed("/Settings");
            },
          ),
          // IconButton(
          //   icon: Icon(Icons.notifications),
          //   onPressed: () {
              
          //   },
          // ),
          // (locale.languageCode=="fa")?
          // new Stack(
          //   children: <Widget>[
          //     new Container(
          //       padding: EdgeInsets.only(top:20,right:10),
          //       child:StreamBuilder<String>(
          //         stream: routerBloc.appTitleObservable,
          //         initialData: "ایران فارما",
          //         builder: (context, snapshot) {
          //           if(snapshot!=null && snapshot.hasData && snapshot.data=="ایران فارما"){
          //             return Text(
          //               snapshot.data,
          //               style: Theme.of(context).appBarTheme.textTheme.headline,
          //             );
          //           }
          //           else{
          //             return Container();
          //           }
          //         }
          //       ),
          //     ),
          //   ],
          // ):Container(),
        ],
      ),
//       floatingActionButton: FloatingActionButton(
//         backgroundColor: Theme.of(context).accentColor,
//         focusColor: Theme.of(context).primaryColorDark,
//         onPressed: () {
//           // do nothing
//           // _counterBloc.decrement();
//         },
//         child: Icon(
//           Icons.done,
// //            color:Theme.of(context).textTheme.subtitle.color,
//         ),
//       ),
      body: Padding(
        padding:EdgeInsets.symmetric(vertical: 1.0,horizontal: 10.0),
          child:CustomScrollView(
          slivers: <Widget>[
            // Add the app bar to the CustomScrollView.
            SliverAppBar(
              // Provide a standard title.
              title: Text("گتابخانه"),
              // Allows the user to reveal the app bar if they begin scrolling
              // back up the list of items.
              floating: false,
              forceElevated: true,
              
              // Display a placeholder widget to visualize the shrinking size.
              flexibleSpace: Placeholder(),
              // Make the initial height of the SliverAppBar larger than normal.
              expandedHeight: 100,
            ),
            // Next, create a SliverList
            SliverList(
              // Use a delegate to build items as they're scrolled on screen.
              delegate: SliverChildBuilderDelegate(
                // The builder function returns a ListTile with a title that
                // displays the index of the current item.
                
                (context, index) => ((index < 1) && ((iw.booksList==null) || (iw.booksList.length==0))) ? CircularProgressIndicator() :  
                SizedBox(
                  height: 60.0,
                  child:Column(
                    children:[
                      ListTile(
                        onTap: (){

                        },
                        
                        leading: (iw.booksList[index].icon != null ) ?LimitedBox(maxHeight: 50.0,child:Image.network(hostName.split("/api")[0]+ iw.booksList[index].icon)) : null,
                        title: iw.booksList[index]!=null  ? Text(iw.booksList[index].title,style:TextStyle(color: iw.booksList[index].id==null ? Theme.of(context).accentColor : Colors.black , fontSize:(iw.booksList[index].id==null)  ? 20.0 :16.0)) : Container(),
                        trailing: (iw.booksList[index].id==null) ? null : IconButton(icon: Icon(CupertinoIcons.forward),onPressed: (){},),
                      ),
                      Divider(
                        height:1.0
                      ),
                    ]
                  ),
                ),
                // Builds 1000 ListTiles
                childCount: (iw.booksList==null || iw.booksList.length==0) ? 1 :  iw.booksList.length,
              ),
            ),
          ],
        ),
      ),
    );
  }

  

  Widget gameBoxWidget(BuildContext context,BookModel bookData){
    return LimitedBox(
      maxHeight: 100.0,
      child:ExpansionTile(
        key: UniqueKey(),
        title: Text(bookData.title),
        children: [
            RaisedButton(
              onPressed: (){

              },child:Text("Join")
            ),
            RaisedButton(
              onPressed: (){

              },child:Text("Create")
            )
        ],
      )
    );
  }

  

  // @override
  // bool get wantKeepAlive => true;


  
}


// NEW NEW NEW NEW NEW
// NEW NEW NEW NEW NEW
class BaseMaterialPageRoute<T> extends MaterialPageRoute<T> {
  Widget result;

  BaseMaterialPageRoute({
    @required WidgetBuilder builder,
    RouteSettings settings,
    bool maintainState = true,
    bool fullscreenDialog = false,
  })  : assert(builder != null),
        assert(maintainState != null),
        assert(fullscreenDialog != null),
        assert(opaque),
        super(
            builder: builder,
            settings: settings,
            maintainState: maintainState,
            fullscreenDialog: fullscreenDialog);

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    if (result == null) {
      result = builder(context);
    }
    assert(() {
      if (result == null) {
        throw FlutterError(
            'The builder for route "${settings.name}" returned null.\n'
            'Route builders must never return null.');
      }
      return true;
    }());
    return Semantics(
      scopesRoute: true,
      explicitChildNodes: true,
      child: result,
    );
  }
}



// GRID 
// body:
//         RefreshIndicator(
//           key: _refreshIndicatorKey,
//           onRefresh: _refresh,
//           child:Material(
//           color:(Theme.of(context).primaryColor==Colors.white) ? Colors.white70 : Colors.white10,
//           child: Padding(
//               padding: const EdgeInsets.all(5.0),
//               child:new GridView.count(
//                 crossAxisCount: (pageWidth >= 1080.0) ? 3 : (pageWidth >= 480.0) ? 2 : 1,
//                 shrinkWrap: true,
//                 childAspectRatio: 1.0,
//                 padding: const EdgeInsets.all(1.0),
//                 mainAxisSpacing: 5.0,
//                 crossAxisSpacing: 5.0,
//                 children:((iw.booksList==null) || (inherittedWidget!=null) && (inherittedWidget.booksList==null || inherittedWidget.booksList.length==0)) ? 
//                   [Container()] : 
//                   (iw.booksList!=[] && iw.booksList.length>0) ? 
//                     new List<TileItem>.generate(iw.booksList.length, (index){
//                       // return TileItem(routerBloc: routerBloc,num: iw.booksList[index].id,gameData:iw.booksList[index]);
//                       new ListTile(
//                         onTap: (){

//                         },
//                         leading: (iw.booksList[index].icon != null ) ?Image.network(hostName.split("/api")[0]+ iw.booksList[index].icon) : Container(),
//                         title: Text(iw.booksList[index].title),
//                         trailing: IconButton(icon: Icon(Icons.arrow_left),onPressed: (){},),
//                       );
//                     // return new GridTile(
//                       // header: Text(booksList[index].name),
//                       // child:gameBoxWidget(context, booksList[index]) //new Image.network(url, fit: BoxFit.cover));
//                     // )
//                   },
//                 )
//                 :
//                 [
//                   this.loader ? Center(
//                       child:CircularProgressIndicator(strokeWidth: 3.0,)
//                     ):Center(
//                       child:Container(
//                         child:Center(child:Text("SHIT")
//                       )
//                     ),
//                   ),
//                 ]
//               ),
//           ),
//         ),
//       ),
      
//     );
//   }

//   Widget gameBoxWidget(BuildContext context,BookModel bookData){
//     return LimitedBox(
//       maxHeight: 100.0,
//       child:ExpansionTile(
//         key: UniqueKey(),
//         title: Text(bookData.title),
//         children: [
//             RaisedButton(
//               onPressed: (){

//               },child:Text("Join")
//             ),
//             RaisedButton(
//               onPressed: (){

//               },child:Text("Create")
//             )
//         ],
//       )
//     );
//   }