import 'package:flutter/cupertino.dart';
import 'package:iranpharma/Repository/accounts.dart';
import 'package:iranpharma/inner_app.dart';
import 'package:iranpharma/localization/localization.dart';
import 'package:iranpharma/widgets/inner_app_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:after_layout/after_layout.dart';
import 'package:iranpharma/widgets/iranpharma_button.dart';

import '../main_state_container.dart';


class ProfilePage extends StatefulWidget {

  

  const ProfilePage({Key key}) : super(key: key);

  @override
  _ProfilePage createState() => new _ProfilePage();
}
class _ProfilePage extends State<ProfilePage> with AfterLayoutMixin<ProfilePage> , RouteAware{ // x   ,AutomaticKeepAliveClientMixin<ProfilePage>
  
  final navigatorKey = GlobalKey<NavigatorState>();
  // final CounterBloc _counterBloc = new CounterBloc(counter: 0);
  String hostName;
  Account accountRepository;
  int x = 0;

  _ProfilePage();
  @override
  void initState() {
    hostName = "http://116.203.231.112/api";
    accountRepository = Account();
    super.initState();
  }


  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(oldWidget) {
    print(" update widget Settings : ");
    super.didUpdateWidget(oldWidget);
  }

  @override
  void afterFirstLayout(BuildContext context) {
   
    // final inheritedWidget = MainStateContainer.of(context);
    // if();
    
    
  }

 

@override
Widget build(BuildContext context) {
  
  final root = mainPage(context);
  final settingsPage ="";
  final serverPage = "";


  final inheritedWidget = MainStateContainer.of(context);
  final locale = inheritedWidget.locale;
  final pageWidth = MediaQuery.of(context).size.width;
  return Scaffold(
    appBar:AppBar(  
      automaticallyImplyLeading:false,
      // centerTitle: true,
      title:Text(
          (inheritedWidget.me!=null) ? DemoLocalizations.of(context).trans("ProfileHeader") : DemoLocalizations.of(context).trans("LoginTitle"),
          style: Theme.of(context).appBarTheme.textTheme.headline,
      ),
      actions: <Widget>[
        inheritedWidget.me!=null ? IconButton(
          icon: Icon(Icons.exit_to_app),
          onPressed: () {
            
            inheritedWidget.authenticationBloc.updateToken(null,0);
            inheritedWidget.updateUserInfo(null);
            // navigatorKey.currentState.pushNamed("/");
            Navigator.popAndPushNamed(context, "/");
          },
        ) : Container(),
            // IconButton(
            //   icon: Icon(Icons.settings),
            //   onPressed: () {
                
            //     // route(context, "/Server");
            //     // Navigator.of(context).pushNamed("/Settings");
            //     // navigatorKey.currentState.pushNamed("/Settings");
            //   },
            // ),
        ],
      ),

      

      body:Navigator(
        key: navigatorKey,
        initialRoute: "/",
        onGenerateRoute: (RouteSettings routeSettings){
          switch (routeSettings.name){
            case "/" : return BaseMaterialPageRoute(builder: (context)=> root);break;
            // case "/Settings" : return MaterialPageRoute( builder: (context)=> settingsPage);break;
            // case "/Server" : return MaterialPageRoute( builder: (context)=> serverPage);break;
            
            // default:{
            //   return BaseMaterialPageRoute(builder: (context)=> drugsPage(context, locale));break;
            // }
            // case 
          }
        })
      );
      
  }

  Widget loginMenu(context){
    return Padding(
      padding: EdgeInsets.only(top:50.0,left:10.0,right: 10.0,bottom: 50.0),
      child:Column(
        children: <Widget>[
          Row(children: <Widget>[
            Expanded(
            flex:4,
            child:SizedBox(
              height: 107.0,
              child:RaisedButton(
                onPressed: (){
                  print("biib");
                },
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topRight:Radius.circular(10.0),  
                    bottomRight: Radius.circular(10.0)
                  ),
                ),
                splashColor: Theme.of(context).primaryColorLight,
                color: Theme.of(context).dialogBackgroundColor,
                child: Icon(
                    Icons.person,size: 50.0,color: Theme.of(context).accentColor,
                  ),
                ),
              ),
            ),
            Expanded(
              flex:9,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children:[ 
                  btnWidget(context,"ورود"),
                  btnWidget(context,"ثبت نام"),
                ]
              ),
            )
          ],
        ),
        Expanded(
          flex:9,
          child:Padding(
            padding:EdgeInsets.only(top:30.0),
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children:[ 
                btnWidget(context,"درباره ما"),
                btnWidget(context,"تماس با ما"),
                btnWidget(context,"قوانین و مقررات"),
              ]
            ),
          )
        ),
      ]
      )
    );
  } 
    

  Widget btnWidget(context,text){
    return Container(
      height: 54.0,
      decoration: BoxDecoration(
        color: Colors.transparent,
      
        borderRadius: BorderRadius.only(
          topLeft: (text=="ورود")? Radius.circular(20.0) : Radius.circular(0.0),
          bottomLeft: (text!="ورود")? Radius.circular(20.0) : Radius.circular(0.0),
        )
      ),
      child: SizedBox(
        height: 54.0,
        child:Row(
          children:[
            Expanded(
              child:SizedBox(
                height: 53.0,
                child:RaisedButton(
                  splashColor: Theme.of(context).primaryColorLight,
                  color: Theme.of(context).dialogBackgroundColor,
                  padding: EdgeInsets.only(top:2.0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      topRight: ((text=="تاس با ما") ||  (text=="درباره ما")) ? Radius.circular(10.0) : Radius.circular(1.0),  
                      bottomLeft: ((text=="قوانین و مقررات") ||  (text=="ثبت نام")) ? Radius.circular(10.0) : Radius.circular(1.0),  
                      bottomRight: ((text=="قوانین و مقررات")) ? Radius.circular(10.0) : Radius.circular(1.0),  
                      topLeft : ((text=="ورود") ||  (text=="درباره ما")) ? Radius.circular(10.0) : Radius.circular(1.0),  
                    ),
                  ),
                  onPressed: (){
                    if(text=="ورود")
                      Navigator.of(navigatorKey.currentContext).pushNamed("/Login");
                    else if(text=="ثبت نام")
                      Navigator.of(navigatorKey.currentContext).pushNamed("/Register");
                    else if(text=="دریاره ما")
                      Navigator.of(navigatorKey.currentContext).pushNamed("/AboutUs");
                    else if(text=="تاس با ما")
                      Navigator.of(navigatorKey.currentContext).pushNamed("/ContactUs");
                    else if(text=="قوانین و مقررات")
                      Navigator.of(navigatorKey.currentContext).pushNamed("/Terms");
                  },
                  child: Text(text,style: TextStyle(fontWeight: FontWeight.w600),),
                ),
              ),
            ),
          ]
        ),
      ),
    );
  }

  Widget mainPage(context){ 
    final inheritedWidget = MainStateContainer.of(context);
    final locale = inheritedWidget.locale;
    final me = inheritedWidget.me;
    final pageWidth = MediaQuery.of(context).size.width;
    
    return Material(
      color:(Theme.of(context).primaryColor==Colors.white) ? Colors.white60 : Colors.white12,
      child:Stack(
        children:[
            Positioned.fill(
              top: (pageWidth > 1400.0 ) ? 130.0 : 190.0,
              right: -30.0,
              child:LimitedBox(
                maxWidth: 800.0,
                child:Image.asset("assets/backgrounds/home2.png",height: 1000.0,width: (pageWidth > 868.0 ) ? 800.0 : null,)
              )
            ),
            Center(
            child: Padding(
              padding: const EdgeInsets.all(0.0),
              child: Padding(
                  padding:EdgeInsets.only(left: (pageWidth > 608.0 ) ? 40.0 : 10.0,right:(pageWidth > 1400.0 ) ? 40.0 :10.0,top:2.0),
                  child: ((me==null) || (me.title=="") || (me.title==null)) ? loginMenu(context) : Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child:Text(
                        DemoLocalizations.of(context).trans("AppTitle"),
                        style: TextStyle(color:Theme.of(context).accentColor,fontSize: 36.0),
                      ),
                    ),
                     Padding(
                      padding: EdgeInsets.symmetric(vertical: 1.0,horizontal: 8.0),
                      child:IranpharmaButton(
                        height: 50.0,
                        width:100.0,
                        radius:4.0,
                        color:Theme.of(context).primaryColor,
                        // onHover:Theme.of(context).accentColor,
                        onSplash: Theme.of(context).primaryColorLight,
                        onPressed:(){},
                        child:Row(children:[Padding(padding:EdgeInsets.all(8.0),child:Icon(CupertinoIcons.person,size: 35.0,color:Theme.of(context).accentColor)),Text(inheritedWidget.me.title,style:TextStyle(color:Colors.black))]),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 1.0,horizontal: 8.0),
                      child:IranpharmaButton(
                        height: 50.0,
                        width:100.0,
                        radius:4.0,
                        color:Theme.of(context).primaryColor,
                        // onHover:Theme.of(context).accentColor,
                        onSplash: Theme.of(context).primaryColorLight,
                        onPressed:(){
                            // this.tabController.animateTo(1);
                            // inheritedWidget.mainRouter.route("/Login");
                            Navigator.pushNamed(context,"/Login");
                            // navigatorKey.currentState.pushNamed("/register");
                        },
                        child:Row(children:[Padding(padding:EdgeInsets.all(8.0),child:Icon(CupertinoIcons.create,size: 30.0,color:Theme.of(context).accentColor)),Text("تغییر اطلاعات پروفایل",style:TextStyle(color:Colors.black))]),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 1.0,horizontal: 8.0),
                      child:IranpharmaButton(
                        height: 50.0,
                        width:100.0,
                        radius:4.0,
                        color:Theme.of(context).primaryColor,
                        // onHover:Theme.of(context).accentColor,
                        onSplash: Theme.of(context).primaryColorLight,
                        onPressed:(){
                            // this.tabController.animateTo(1);
                            // inheritedWidget.mainRouter.route("/Login");
                            Navigator.pushNamed(context,"/Login");
                            
                            // navigatorKey.currentState.pushNamed("/register");
                        },
                        child:Row(children:[Padding(padding:EdgeInsets.all(8.0),child:Icon(Icons.lock_outline,size: 33.0,color:Theme.of(context).accentColor),),Text("تغییر کلمه عبور",style:TextStyle(color:Colors.black))]),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 1.0,horizontal: 8.0),
                      child:IranpharmaButton(
                        height: 50.0,
                        width:100.0,
                        radius:4.0,
                        color:Theme.of(context).primaryColor,
                        // onHover:Theme.of(context).accentColor,
                        onSplash: Theme.of(context).primaryColorLight,
                        onPressed:(){
                            // this.tabController.animateTo(1);
                            Navigator.pushNamed(context,"/Register");
                            // inheritedWidget.mainRouter.route("/Register");
                        },
                        child:Row(children:[Padding(padding:EdgeInsets.all(8.0),child:Icon(Icons.eject,size: 33.0,color:Theme.of(context).accentColor),),Text("خروج از حساب کاربری",style:TextStyle(color:Colors.black))]),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ]
      ),
    );
  }


  // @override
  // bool get wantKeepAlive => true;
  

}