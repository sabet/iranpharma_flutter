import 'dart:io';
import 'package:iranpharma/Repository/accounts.dart';
import 'package:iranpharma/localization/localization.dart';
import 'package:iranpharma/pages/games.dart';
import 'package:iranpharma/server_page.dart';
import 'package:iranpharma/settings_page.dart';
import 'package:iranpharma/widgets/inner_app_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:after_layout/after_layout.dart';
import 'package:iranpharma/widgets/iranpharma_button.dart';

import '../main_state_container.dart';


class HomePage extends StatefulWidget {

  const HomePage({Key key}) : super(key: key);

  @override
  _HomePage createState() => new _HomePage();
}
class _HomePage extends State<HomePage> with AfterLayoutMixin<HomePage> , RouteAware{ // 
  
  // final CounterBloc _counterBloc = new CounterBloc(counter: 0);
  int x = 0;
  String hostName;
  Account accountRepository;
  bool loader;
  var routeObserverObject;
  BuildContext lastContext;
  final navigatorKey = GlobalKey<NavigatorState>();

  _HomePage();
  @override
  void initState() {
    
    hostName = "http://116.203.231.112/api";
    accountRepository = Account();
    super.initState();
  }


  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(oldWidget) {
    print(" update widget Settings : ");
    super.didUpdateWidget(oldWidget);
  }

  @override
  void afterFirstLayout(BuildContext context) {
   
    final inheritedWidget = MainStateContainer.of(context);
    // if();
    
    
  }

 

@override
Widget build(BuildContext context) {
  
    final root = mainPage(context);
    final settingsPage ="";
    final serverPage = "";


  final inheritedWidget = MainStateContainer.of(context);
  final locale = inheritedWidget.locale;
  final pageWidth = MediaQuery.of(context).size.width;
  return Scaffold(
    appBar:AppBar(  
      automaticallyImplyLeading:false,
      // centerTitle: true,
      title:Text(
          DemoLocalizations.of(context).trans("HomeHeader"),
          style: Theme.of(context).appBarTheme.textTheme.headline,
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.exit_to_app),
          onPressed: () {
            
            // inheritedWidget.authenticationBloc.updateToken(null,0);
            // inheritedWidget.updateUserInfo(null);
            // // navigatorKey.currentState.pushNamed("/");
            // Navigator.popAndPushNamed(context, "/");
          },
        ),
            // IconButton(
            //   icon: Icon(Icons.settings),
            //   onPressed: () {
                
            //     // route(context, "/Server");
            //     // Navigator.of(context).pushNamed("/Settings");
            //     // navigatorKey.currentState.pushNamed("/Settings");
            //   },
            // ),
        ],
      ),

      

      body:Navigator(
        key: navigatorKey,
        initialRoute: "/",
        onGenerateRoute: (RouteSettings routeSettings){
          switch (routeSettings.name){
            case "/" : return BaseMaterialPageRoute(builder: (context)=> root);break;
            // case "/Settings" : return MaterialPageRoute( builder: (context)=> settingsPage);break;
            // case "/Server" : return MaterialPageRoute( builder: (context)=> serverPage);break;
            
            // default:{
            //   return BaseMaterialPageRoute(builder: (context)=> drugsPage(context, locale));break;
            // }
            // case 
          }
        })
      );
      
  }

    


  Widget mainPage(context){ 
    final inheritedWidget = MainStateContainer.of(context);
    final locale = inheritedWidget.locale;
    final me = inheritedWidget.me;
    final pageWidth = MediaQuery.of(context).size.width;
    
    return Material(
      color:(Theme.of(context).primaryColor==Colors.white) ? Colors.white60 : Colors.white12,
      child:Stack(
        children:[
            Positioned.fill(
              top: (pageWidth > 1400.0 ) ? 130.0 : 190.0,
              right: -30.0,
              child:LimitedBox(
                maxWidth: 800.0,
                child:Image.asset("assets/backgrounds/home2.png",height: 1000.0,width: (pageWidth > 868.0 ) ? 800.0 : null,)
              )
            ),
            Center(
            child: Padding(
              padding: const EdgeInsets.all(0.0),
              child: 
                    Padding(
                      padding:EdgeInsets.only(left: (pageWidth > 608.0 ) ? 40.0 : 10.0,right:(pageWidth > 1400.0 ) ? 40.0 :10.0,top:10.0),
                      child:Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(10.0),
                          child:Text(
                            DemoLocalizations.of(context).trans("AppTitle"),
                            style: TextStyle(color:Theme.of(context).accentColor,fontSize: 36.0),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child:IranpharmaButton(
                            height: 50.0,
                            width:100.0,
                            radius:20.0,
                            onHover:Theme.of(context).accentColor,
                            color:Theme.of(context).accentColor,
                            onSplash: Theme.of(context).primaryColor,
                            onPressed:(){
                                // this.tabController.animateTo(1);
                                // navigatorKey.currentState.pushNamed("/");
                                
                            },
                            child:Row(children:[
                              Padding(
                                padding:EdgeInsets.all(8.0),
                                child:Icon(Icons.info,color:Colors.white
                                )
                              ),
                              ((me==null) || (me.title=="") || (me.title==null)) ? Text("ورود",style:TextStyle(color:Colors.white)) : Text( me.title,style:TextStyle(color:Colors.white)) 
                              ]
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(7.0),
                          child:IranpharmaButton(
                            height: 50.0,
                            width:100.0,
                            radius:20.0,
                            color:Theme.of(context).primaryColor,
                            onHover:Theme.of(context).accentColor,
                            onSplash: Theme.of(context).primaryColorLight,
                            onPressed:(){
                                // this.tabController.animateTo(1);
                                // inheritedWidget.mainRouter.route("/Login");
                                Navigator.pushNamed(context,"/Login");
                                // navigatorKey.currentState.pushNamed("/register");
                            },
                            child:Row(children:[Padding(padding:EdgeInsets.all(8.0),child:Icon(Icons.star,color:Colors.black)),Text("دارو های نشان شده",style:TextStyle(color:Colors.black))]),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(7.0),
                          child:IranpharmaButton(
                            height: 50.0,
                            width:100.0,
                            radius:20.0,
                            color:Theme.of(context).primaryColor,
                            onHover:Theme.of(context).accentColor,
                            onSplash: Theme.of(context).primaryColorLight,
                            onPressed:(){
                                // this.tabController.animateTo(1);
                                inheritedWidget.mainRouter.route("/Register");
                            },
                            child:Row(children:[Padding(padding:EdgeInsets.all(8.0),child:Icon(Icons.access_time,color:Colors.black)),Text("آخرین دارو های مشاهده شده",style:TextStyle(color:Colors.black))]),
                          ),
                        ),
                      ],
                    ),
                  ),
                
            ),
          ),
        ]
      ),
    );
  }


 
}
