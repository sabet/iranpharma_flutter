import 'dart:io';
import 'package:iranpharma/animationControllers/tedy_controller.dart';
import 'package:iranpharma/animationControllers/switch_controller.dart';
import 'package:iranpharma/blocs/authentication_bloc.dart';
import 'package:iranpharma/localization/localization.dart';
import 'package:iranpharma/main_state_container.dart';
import 'package:iranpharma/widgets/signin_button.dart';
import 'package:flutter/material.dart';
import 'package:iranpharma/blocs/theme_bloc.dart';
import 'package:iranpharma/blocs/router_bloc.dart';
import 'package:iranpharma/widgets/inner_app_bar_widget.dart';
import 'package:flutter/rendering.dart';
import 'package:after_layout/after_layout.dart';
import 'package:iranpharma/trackings/tracking_text_input.dart';
import 'package:iranpharma/Repository/accounts.dart';
// import 'package:flare_flutter/flare_actor.dart';



class LoginPage extends StatefulWidget {
  final ThemeBloc themeBloc;
  final RouterBloc routerBloc;
  final ThemeData themeData;
  final AuthenticationBloc authenticationBloc;
  //final RouteObserver<PageRoute> routeObserver;

  const LoginPage({Key key, this.themeBloc, this.routerBloc, this.themeData,this.authenticationBloc}) : super(key: key);

  @override
  _LoginPage createState() => new _LoginPage(this.themeBloc,this.routerBloc,this.themeData,this.authenticationBloc);
}
class _LoginPage extends State<LoginPage> with AfterLayoutMixin<LoginPage> , RouteAware{
  final ThemeBloc themeBloc;
  final ThemeData themeData;
  final RouterBloc routerBloc;
  bool loader;
  final Account accountRepository = Account();
//  final RouteObserver<PageRoute> routeObserver;
  BuildContext lastContext;
  final AuthenticationBloc authenticationBloc;
  TeddyController _teddyController;
  SwitchController _switchController;
  
  // final CounterBloc _counterBloc = new CounterBloc(counter: 0);
  int x = 0;

  _LoginPage(this.themeBloc,this.routerBloc, this.themeData,this.authenticationBloc);
  @override
  void initState() {
    loader = false;
    _teddyController=TeddyController();
    // _switchController = SwitchController();
    super.initState();
  }


  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(oldWidget) {
    print(" update widget Settings : ");
    super.didUpdateWidget(oldWidget);
  }

  @override
  void afterFirstLayout(BuildContext context) {
    
  }

//  @override
//  void didChangeDependencies(){
//    print("Change dependenncies home tabs : ");
//    super.deactivate();
////    super.didChangeDependencies();
////    dispose();
////    this.dispose();
//  }


  Future<dynamic> getMe(BuildContext context) async{
    final inheritedWidget = MainStateContainer.of(context);
    final hostName = inheritedWidget.hostName;
    var response = accountRepository.getMe(hostName+"/profile");
    response.timeout(Duration(seconds:5));
    return response;  
  }

  @override
  Widget build(BuildContext context) {
    EdgeInsets devicePadding = MediaQuery.of(context).padding;
    // var pageHeight = MediaQuery.of(context).size.height;
    var pageWidth = MediaQuery.of(context).size.width;
    final inheritedWidget = MainStateContainer.of(context);
    final hostName = inheritedWidget.hostName;
    final locale = inheritedWidget.locale;
    final title = Padding(
            padding: EdgeInsets.only(right: 5.0,top:5.0),
            child:Text(
                  DemoLocalizations.of(context).trans("LoginTitle").toString(),
                  style: Theme.of(context).textTheme.title,
                )
            );
    

    final innerHeaderActions = Row(
      children:<Widget> [
        (locale.languageCode=='fa'||locale.languageCode=='ar') ? 
          Padding(
            padding: (locale.languageCode=='fa'||locale.languageCode=='ar') ? EdgeInsets.only(right: 5.0,top:10.0):EdgeInsets.only(left: 10.0,top:0.0),
            child:Icon(Icons.keyboard,size: 35,color:Theme.of(context).accentColor,)
          )
          :Container(),
          (locale.languageCode=='fa'||locale.languageCode=='ar') ? title : Container(),
          //  Padding(
          //   padding: EdgeInsets.only(right: 20.0,top:5.0),
          //   child:IconButton(
          //     icon: new Icon(Icons.keyboard,size:35),
          //     tooltip: 'shit',
          //     color: Theme.of(context).accentColor,
          //     focusColor: Theme.of(context).primaryColorDark,
          //     onPressed: () => _RightButtonPressed(context),
          //   ),      
          //  ),
        ]
    );


    final innerHeaderLeading = RotatedBox(
        quarterTurns: (locale.languageCode=='fa'||locale.languageCode=='ar') ? 2 : 0,
        child:IconButton(icon: new Icon(Icons.arrow_back_ios,color:Theme.of(context).accentColor),
      tooltip: 'Back',
      onPressed: () => _backButtonPressed(),
    ));

    final focus = FocusNode();
    final focus1 = FocusNode();
    return Scaffold(
      // resizeToAvoidBottomInset:false,
      appBar: AppBar(  
      automaticallyImplyLeading:true,
      // centerTitle: true,
      title:Text(
          DemoLocalizations.of(context).trans("LoginTitle"),
          style: Theme.of(context).appBarTheme.textTheme.headline,
      ),
      actions: <Widget>[
        
        ],
      ),
      backgroundColor: Color.fromRGBO(93, 142, 155, 1.0),
      body: Container(
        child:Stack(
        children: <Widget>[
          Positioned.fill(
              child: Container(
                decoration: BoxDecoration(
                  // Box decoration takes a gradient
                  color: Theme.of(context).dialogBackgroundColor,
                  // gradient: LinearGradient(
                  //   // Where the linear gradient begins and ends
                  //   begin: Alignment.topCenter,
                  //   end: Alignment.bottomCenter,
                  //   // Add one stop for each color. Stops should increase from 0 to 1
                  //   stops: [0.0,0.7,1.0],
                  //   colors: [
                  //     Color.fromRGBO(255,255,255, 1.0),
                  //     Color.fromRGBO(0,0,150, 1.0),
                  //     Color.fromRGBO(0, 0, 150, 1.0),
                  //   ],
                  // ),
                ),
              )
            ),
            Center(
              child:Container(
                child: SingleChildScrollView(
                  padding: EdgeInsets.only(
                    left: 20.0, right: 20.0, top: 2.0,bottom:10.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: 0.0,// 1100 
                          width: 250.0,
                          padding: const EdgeInsets.only(left: 30.0, right:30.0),
                          child:Container(),
                          // child:FlareActor(2
                          //   "assets/animations/Teddy.flr",
                          //   alignment: Alignment.bottomCenter,
                          //   fit: BoxFit.fill,
                          //   controller: _teddyController,
                          // ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color:Theme.of(context).scaffoldBackgroundColor,
                            borderRadius:
                            BorderRadius.all(Radius.circular(7.0)
                            ),     
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey[400],
                                spreadRadius: 1,
                                blurRadius: 1,
                                offset: Offset(0, 0.5), // changes position of shadow
                              ),
                            ],
                          ),
                          width: (pageWidth >= 1400.0) ? 400.0 : 300.0,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical:50.0,horizontal: 30.0),
                            child: Form(
                              child: Container(
                                child:Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    TrackingTextInput(
                                      focus: focus,
                                      targetFocus: focus1,
                                      inputAction: TextInputAction.next,
                                      label: DemoLocalizations.of(context).trans('UsernameText'),
                                      hint: DemoLocalizations.of(context).trans('UsernamePlaceholder'),
                                      onCaretMoved: (Offset caret) {
                                        // _teddyController.lookAt(caret);
                                      },
                                      onTextChanged: (String text){
                                        authenticationBloc.updateUsername(text);
                                      },
                                  ),
                                  TrackingTextInput(
                                    focus:focus1,
                                    inputAction: TextInputAction.done,
                                    label: DemoLocalizations.of(context).trans('PasswordText'),
                                    hint: "*****",
                                    isObscured: true,
                                    onCaretMoved: (Offset caret) {
                                      // _teddyController.coverEyes(caret != null);
                                      // _teddyController.lookAt(null);
                                      //                                          _teddyController.coverEyes(false);
                                    },
                                    onTextChanged: (String value) {
                                      // _teddyController.coverEyes(true);
                                      authenticationBloc.updatePassword(value);
                                    },
                                  ),
                                  // SizedBox(
                                  //   height:60.0,
                                  //   width:150.0,
                                  //   child:RaisedButton(
                                  //     color: Theme.of(context).backgroundColor,
                                      
                                  //     onPressed:(){
                                  //       if(this.x==1){
                                  //         setState(() {
                                  //           x = 0;
                                  //         });
                                  //         _teddyController.play("fail");
                                          
                                  //       }
                                  //       else{
                                  //         setState(() {
                                  //           x = 1;
                                  //         });
                                  //         _teddyController.play("success");
                                          
                                  //       }
                                  //     },
                                  //     child:FlareActor(
                                  //       "assets/animations/switch_daytime.flr",
                                  //       alignment: Alignment.bottomCenter,
                                  //       fit: BoxFit.fill,
                                  //       controller: _switchController,
                                  //     ),
                                  //   ),
                                  // ),
                                  
                                  SigninButton(
                                      child: (loader) ? CircularProgressIndicator(
                                        backgroundColor: Colors.white,
                                      ):
                                      Text(DemoLocalizations.of(context).trans('LoginButton'),
                                        style: TextStyle(color:Colors.white,fontWeight: FontWeight.w600)
                                      ),
                                      onPressed: () {
                                        //                                          _teddyController.coverEyes(false);
                                        //                                          _teddyController.lookAt(null);

                                        // if(authenticationBloc.getKey()!=null){
                                          
                                          Future.delayed(Duration(milliseconds: 10),(){
                                            setState(() {
                                              loader = true;
                                            });
                                            _teddyController.submitPassword(
                                              authenticationBloc.getUsername(),
                                              authenticationBloc.getPassword(),
                                              hostName+"/auth/login").
                                              then((res) async{
                                                setState((){
                                                  loader = false;
                                                });
                                                var err = "";
                                                if(res["message"] != null ){
                                                  if(res["message"] == "Unauthorized"){
                                                    err = DemoLocalizations.of(context).trans(res["message"]+"Attempt");
                                                  }
                                                  else{
                                                    err = res["message"];
                                                  }
                                                  Scaffold.of(context).showSnackBar(
                                                    new SnackBar(
                                                      backgroundColor: Colors.red,
                                                    content: Text(err),
                                                  ));
                                                  authenticationBloc.updatePassword(""); // some one else on parents like main must listen to token and user change , split this shit up ,
                                                }
                                                else{
                                                  
                                                  authenticationBloc.updateToken(res["access_token"],res["expires_in"]); // some one else on parents like main must listen to token and user change , split this shit up ,
                                                  dynamic user = await getMe(context);
                                                  
                                                  final wellcomeFullText = (inheritedWidget.locale.languageCode=="ar" || inheritedWidget.locale.languageCode=="fa" ) ? (DemoLocalizations.of(context).trans("WellComeLogin").toString() + " " + user["title"].toString())  :   (user["title"].toString()  + "" + DemoLocalizations.of(context).trans("WellComeLogin").toString()) ;
                                                  Scaffold.of(context).showSnackBar(
                                                    new SnackBar(
                                                      backgroundColor: Colors.green,
                                                    content: Text(wellcomeFullText,textAlign: TextAlign.right,),
                                                  ));
                                                  inheritedWidget.updateUserInfo(user);
                                                  
                                                  _backButtonPressed();
                                                }
                                              }
                                            );
                                          });
                                          
                                    
                                    },
                                  )
                                ],
                              )
                            ),
                          )
                        ),
                      ),
                    ]
                  )
                ),
              ),
            ),
          ],
        ),
      )
    );
  }
  void _backButtonPressed(){
    if(Navigator.canPop(context)){
      Navigator.of(context).pop();
      if(Navigator.canPop(context)){
        Navigator.popAndPushNamed(context, "/");
      }
    }
  }

  Widget _rightButtonPressed(context){
    // return AlertDialog(title: Text("shit",style:Theme.of(context).textTheme.subhead));
  }


}
