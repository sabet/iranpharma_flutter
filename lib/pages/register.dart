import 'dart:convert';
import 'dart:io';
import 'package:iranpharma/Repository/accounts.dart';
import 'package:iranpharma/animationControllers/tedy_controller.dart';
import 'package:iranpharma/blocs/authentication_bloc.dart';
import 'package:iranpharma/widgets/inner_app_bar_widget.dart';
import 'package:iranpharma/widgets/signin_button.dart';
import 'package:flutter/material.dart';
import 'package:iranpharma/blocs/theme_bloc.dart';
import 'package:iranpharma/blocs/router_bloc.dart';
import 'package:flutter/rendering.dart';
import 'package:iranpharma/localization/localization.dart';
import 'package:after_layout/after_layout.dart';
import 'package:iranpharma/trackings/tracking_text_input.dart';
// import 'package:flare_flutter/flare_actor.dart';

import '../main_state_container.dart';


class RegisterPage extends StatefulWidget {
  final ThemeBloc themeBloc;
  final RouterBloc routerBloc;
  final ThemeData themeData;
  final AuthenticationBloc authenticationBloc;
  const RegisterPage({Key key, this.themeBloc, this.routerBloc, this.themeData, this.authenticationBloc}) : super(key: key);

  @override
  _RegisterPage createState() => new _RegisterPage(this.themeBloc,this.routerBloc,this.themeData,this.authenticationBloc);
}
class _RegisterPage extends State<RegisterPage> with AfterLayoutMixin<RegisterPage> , RouteAware{
  final ThemeBloc themeBloc;
  final ThemeData themeData;
  final RouterBloc routerBloc;
  final AuthenticationBloc authenticationBloc;
  BuildContext lastContext;
  TeddyController _teddyController;
  final Account accountRepository = Account();
  // final CounterBloc _counterBloc = new CounterBloc(counter: 0);
  int x = 0;

  _RegisterPage(this.themeBloc,this.routerBloc, this.themeData, this.authenticationBloc);
  @override
  void initState() {
    
   _teddyController=TeddyController();
    super.initState();
  }


  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(oldWidget) {
    print(" update widget Settings : ");
    super.didUpdateWidget(oldWidget);
  }

  @override
  void afterFirstLayout(BuildContext context) {
   

    
  }


//  @override
//  void didChangeDependencies(){
//    print("Change dependenncies home tabs : ");
//    super.deactivate();
////    super.didChangeDependencies();
////    dispose();
////    this.dispose();
//  }

 getMe(BuildContext context){
    final inheritedWidget = MainStateContainer.of(context);
    final hostName = inheritedWidget.hostName;
    var response = accountRepository.getMe(hostName+"/profile");
    response.then((user){
      inheritedWidget.updateUserInfo(user);
    }).catchError((err){
      print(err);
    }).timeout(Duration(seconds:2));
    

  }

  @override
  Widget build(BuildContext context) {
    EdgeInsets devicePadding = MediaQuery.of(context).padding;
    // var pageHeight = MediaQuery.of(context).size.height;
    var pageWidth = MediaQuery.of(context).size.width;
    final validCharacters = RegExp(r'^[a-zA-Z0-9_\-=@,\.;]+$');
    final inheritedWidget = MainStateContainer.of(context);
    final locale = inheritedWidget.locale;
    final title = Padding(
            padding: EdgeInsets.only(right: 5.0,top:5.0),
            child:Text(
                  DemoLocalizations.of(context).trans("RegisterTitle").toString(),
                  style: Theme.of(context).textTheme.title,
                )
            );
    final hostName = inheritedWidget.hostName;

    final innerHeaderActions = Row(
      children:<Widget> [
        (locale.languageCode=='fa'||locale.languageCode=='ar') ? 
          Padding(
            padding: (locale.languageCode=='fa'||locale.languageCode=='ar') ? EdgeInsets.only(right: 5.0,top:10.0):EdgeInsets.only(left: 10.0,top:0.0),
            child:Icon(Icons.keyboard,size: 35,color:Theme.of(context).accentColor)
          )
          :Container(),
          (locale.languageCode=='fa'||locale.languageCode=='ar') ? title : Container(),
          //  Padding(
          //   padding: EdgeInsets.only(right: 20.0,top:5.0),
          //   child:IconButton(
          //     icon: new Icon(Icons.keyboard,size:35),
          //     tooltip: 'shit',
          //     color: Theme.of(context).accentColor,
          //     focusColor: Theme.of(context).primaryColorDark,
          //     onPressed: () => _RightButtonPressed(context),
          //   ),      
          //  ),
        ]
    );


    final innerHeaderLeading = RotatedBox(
        quarterTurns: (locale.languageCode=='fa'||locale.languageCode=='ar') ? 2 : 0,
        child:IconButton(icon: new Icon(Icons.arrow_back_ios,color:Theme.of(context).accentColor),
      tooltip: 'Back',
      onPressed: () => _backButtonPressed(),
    ));
    final focusUsername = FocusNode();
    final focusEmail = FocusNode();
    final focusPassword = FocusNode();
    final focusConfPassword = FocusNode();
    final focusFullName = FocusNode();
    return Scaffold(
      // resizeToAvoidBottomInset:false,
      appBar: AppBar(  
      automaticallyImplyLeading:true,
      // centerTitle: true,
      title:Text(
          DemoLocalizations.of(context).trans("RegisterTitle"),
          style: Theme.of(context).appBarTheme.textTheme.headline,
      ),
      actions: <Widget>[
        
        ],
      ),
      backgroundColor: Color.fromRGBO(93, 142, 155, 1.0),
      body: Container(
        child:Stack(
        children: <Widget>[
          Positioned.fill(
              child: Container(
                decoration: BoxDecoration(
                  // Box decoration takes a gradient
                  color: Theme.of(context).dialogBackgroundColor,
                  // gradient: LinearGradient(
                  //   // Where the linear gradient begins and ends
                  //   begin: Alignment.topCenter,
                  //   end: Alignment.bottomCenter,
                  //   // Add one stop for each color. Stops should increase from 0 to 1
                  //   stops: [0.0,0.7,1.0],
                  //   colors: [
                  //     Color.fromRGBO(255,255,255, 1.0),
                  //     Color.fromRGBO(0,0,150, 1.0),
                  //     Color.fromRGBO(0, 0, 150, 1.0),
                  //   ],
                  // ),
                ),
              )
            ),
            Center(
              child:Container(
                child: SingleChildScrollView(
                  padding: EdgeInsets.only(
                    left: 20.0, right: 20.0, top: 20.0,bottom:10.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: 0.0,// 1100 
                          width: 250.0,
                          padding: const EdgeInsets.only(left: 30.0, right:30.0),
                          child:Container(),
                          // child:FlareActor(2
                          //   "assets/animations/Teddy.flr",
                          //   alignment: Alignment.bottomCenter,
                          //   fit: BoxFit.fill,
                          //   controller: _teddyController,
                          // ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color:Theme.of(context).scaffoldBackgroundColor,
                            borderRadius:
                            BorderRadius.all(Radius.circular(7.0)
                            ),     
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey[400],
                                spreadRadius: 1,
                                blurRadius: 1,
                                offset: Offset(0, 0.5), // changes position of shadow
                              ),
                            ],
                          ),
                          width: (pageWidth >= 1400.0) ? 400.0 : 300.0,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical:50.0,horizontal: 30.0),
                            child: Form(
                              child: Container(
                                child:Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    StreamBuilder<String>(
                                      stream: authenticationBloc.phoneNumberErrorObserver,
                                      builder: (context, snapshot) {
                                        return TrackingTextInput(
                                          targetFocus: focusFullName,
                                          focus:focusUsername,
                                          inputAction: TextInputAction.next,
                                          label: (
                                              (snapshot.data=="" || snapshot.data==null) ? DemoLocalizations.of(context).trans('PhoneText').toString(): snapshot.data
                                          ),
                                          icon:(snapshot.data==null),
                                          name:"phone",
                                          authenticationBloc: authenticationBloc,
                                          observable: authenticationBloc.phoneNumberErrorObserver,
                                          // hint: DemoLocalizations.of(context).trans('PhoneText').toString(),
                                          onCaretMoved: (Offset caret) {
                                            // _teddyController.lookAt(caret);
                                          },
                                          keyboardType: TextInputType.number,
                                          onTextChanged: (String text){
                                            authenticationBloc.updateUsername(text);
                                            
                                          },
                                        );
                                      }
                                    ),
                                    TrackingTextInput(
                                      focus: focusFullName,
                                      targetFocus:focusEmail,
                                      inputAction: TextInputAction.next,
                                      label:DemoLocalizations.of(context).trans('FullNameText').toString(),
                                      name:"fullname",
                                      keyboardType: TextInputType.text,
                                      authenticationBloc: authenticationBloc,
                                      // observable: authenticationBloc.passwordErrorObserver,
                                      onTextChanged: (String text) {
                                        authenticationBloc.updateFirstname(text);
                                        
                                      },
                                    ),
                                    StreamBuilder<String>(
                                      stream: authenticationBloc.emailErrorObserver,
                                      builder: (context, snapshot) {
                                        if(snapshot!=null){
                                          return TrackingTextInput(
                                            targetFocus: focusPassword,
                                            focus:focusEmail,
                                            inputAction: TextInputAction.next,
                                            label: (
                                              (snapshot.data=="" || snapshot.data==null) ? DemoLocalizations.of(context).trans('EmailText').toString(): snapshot.data
                                            ),
                                            icon:(snapshot.data==null),
                                            authenticationBloc: authenticationBloc,
                                            observable: authenticationBloc.emailErrorObserver,
                                            hint: DemoLocalizations.of(context).trans(DemoLocalizations.of(context).trans('EmailPlaceholderReg')),
                                            onCaretMoved: (Offset caret) {
                                              // _teddyController.lookAt(caret);
                                            },
                                            name:"email",
                                            keyboardType: TextInputType.emailAddress,
                                            onTextChanged: (String text){
                                              authenticationBloc.updateEmail(text);
                                              var emailValid = RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(text);
                                              if((text.length > 4) && emailValid) {
                                                // indicator trigger
                                              //   authenticationBloc.setError("email", "loading");
                                              //   accountRepository.checkIsTaken(hostName+"/api/v1/register/checkIsTaken",{
                                              //     "text": text,
                                              //     "type": "email",
                                              //     "key": authenticationBloc.getKey(),
                                              //   }).then((res) {
                                              //     if(res["result"])
                                              //       authenticationBloc.setError("email","");
                                              //     else {
                                              //       // authenticationBloc.setError("email", DemoLocalizations.of(context).trans("EmailTaken").toString());
                                              //     }
                                              //     // indicator finish
                                              //   });
                                              // }
                                              // else{
                                              //   // authenticationBloc.setError("email", DemoLocalizations.of(context).trans("InvalidEmail").toString());
                                              }
                                            },
                                          );
                                        }
                                        else{
                                          return Container();
                                        }
                                      }
                                    ),

                                  StreamBuilder<String>(
                                    stream: authenticationBloc.passwordErrorObserver,
                                    builder: (context, snapshot) {
                                      return TrackingTextInput(
                                        focus: focusPassword,
                                        targetFocus:focusConfPassword,
                                        inputAction: TextInputAction.next,
                                        label:(
                                          (snapshot.data=="" || snapshot.data==null) ? DemoLocalizations.of(context).trans('PasswordText').toString(): snapshot.data
                                        ),
                                        hint: "",
                                        keyboardType: TextInputType.text,
                                        icon:(snapshot.data==null),
                                        isObscured: true,
                                        onCaretMoved: (Offset caret) {
                                          // _teddyController.coverEyes(caret != null);
                                          // // _teddyController.lookAt(null);
                                          // // _teddyController.coverEyes(false);
                                        },
                                        name:"password",
                                        authenticationBloc: authenticationBloc,
                                        observable: authenticationBloc.passwordErrorObserver,
                                        onTextChanged: (String text) {
                                          authenticationBloc.updatePassword(text);
                                          if((text.length > 5)) {
                                            // indicator trigger
                                            authenticationBloc.setError("password","");
                                          }
                                          else{
                                            // authenticationBloc.setError("password", DemoLocalizations.of(context).trans("PasswordError").toString());
                                          }
                                        },
                                      );
                                    }
                                  ),
                                  StreamBuilder<String>(
                                    stream: authenticationBloc.confPasswordErrorObserver,
                                    builder: (context, snapshot) {
                                      return TrackingTextInput(
                                        focus: focusConfPassword,
                                        targetFocus: focusConfPassword,
                                        inputAction: TextInputAction.done,
                                        label:(
                                          (snapshot.data=="" || snapshot.data==null) ? DemoLocalizations.of(context).trans('ConfPasswordText').toString(): snapshot.data
                                        ),
                                        hint: "",
                                        icon:(snapshot.data==null),
                                        isObscured: true,
                                        onCaretMoved: (Offset caret) {
                                          // _teddyController.coverEyes(caret != null);
                                          // _teddyController.lookAt(null);
                                          //                                          _teddyController.coverEyes(false);
                                        },
                                        name:"confPassword",
                                        keyboardType: TextInputType.text,
                                        authenticationBloc: authenticationBloc,
                                        observable: authenticationBloc.confPasswordErrorObserver,
                                        onTextChanged: (String text){
                                          authenticationBloc.updateConfPass(text);
                                          
                                            // indicator trigger
                                            if(authenticationBloc.password.toString()==text){
                                              authenticationBloc.setError("confPassword","");
                                            }
                                            else{
                                                authenticationBloc.setError("confPassword",DemoLocalizations.of(context).trans("ConfPasswordError").toString());
                                            }
                                          
                                          
                                        }
                                      );
                                    }
                                  ),
                                  SigninButton(
                                    child: Text(DemoLocalizations.of(context).trans("RegisterButton").toString(),
                                        style: TextStyle(
                                            // fontFamily: "RobotoMedium",
                                            fontSize: 16,
                                            color: Colors.white
                                        )
                                    ),
                                    onPressed: () {
                                      //                                          _teddyController.coverEyes(false);
                                      //                                          _teddyController.lookAt(null);
                                      
                                        // send verify req , on button next page send register shit :D // open modal
                                        _teddyController.submitRegisterForm(
                                          authenticationBloc.getUsername(),
                                          authenticationBloc.getEmail(),
                                          authenticationBloc.getFirstName(),
                                          authenticationBloc.getLastName(),
                                          authenticationBloc.getPassword(),
                                          authenticationBloc.getConfPass(),
                                          hostName+"/auth/user_register"
                                        )
                                        .then((res){
                                          if(res.startsWith("ERR")){
                                            Scaffold.of(context).showSnackBar(
                                              new SnackBar(
                                                backgroundColor: Theme.of(context).primaryColor,
                                              content: Text(res.split(":")[1].toString()),
                                            ));
                                          }
                                          else{
                                            authenticationBloc.updateToken(res,0); // some one else on parents like main must listen to token and user change , split this shit up ,
                                            inheritedWidget.setToken(res);
                                            getMe(context);
                                            routerBloc.route("/");
                                            Scaffold.of(context).showSnackBar(
                                              new SnackBar(
                                                backgroundColor: Theme.of(context).primaryColor,
                                              content: Text("Wellcome To Boardgame Baz"),
                                            ));
                                            // _teddyController.coverEyes(false);
                                          }
                                        }
                                      );
                                    
                                    },
                                  )
                                ],
                              )
                            ),
                          )
                        ),
                      ),
                    ]
                  )
                ),
              ),
            ),
          ],
        ),
      )
    );
  }
  void _backButtonPressed(){
    routerBloc.route("/");
  }

 
}
