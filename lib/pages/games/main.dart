
import 'package:iranpharma/Repository/games.dart';
import 'package:iranpharma/blocs/router_bloc.dart';
import 'package:iranpharma/localization/localization.dart';
import 'package:iranpharma/models/books.model.dart';
import 'package:iranpharma/widgets/iranpharma_button.dart';
import 'package:iranpharma/widgets/inner_app_bar_widget.dart';
import 'package:iranpharma/widgets/tile_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:after_layout/after_layout.dart';
import '../../main_state_container.dart';


class GamesMain extends StatefulWidget {
  
  final Games gamesRepository;
  final String name;
  final RouterBloc chatRouterBloc;
  final int id;
  final TabController tabController;

  const GamesMain({Key key,this.gamesRepository, this.name, this.id,this.tabController, this.chatRouterBloc}) : super(key: key);

  @override
  _GamesMain createState() => new _GamesMain(this.gamesRepository,this.name, this.id,this.tabController,this.chatRouterBloc);
}
class _GamesMain extends State<GamesMain> with AfterLayoutMixin<GamesMain>{ //

  final Games gamesRepository;
  final TabController tabController;
  final RouterBloc chatRouterBloc;
  StateContainerState inherittedWidget;
  String hostName;
  final String name;
  final int id;
  
  
  
  int x = 0;
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = new GlobalKey<RefreshIndicatorState>();
  _GamesMain(this.gamesRepository,this.name, this.id,this.tabController, this.chatRouterBloc);
  
  @override
  void initState() {
    
    
    super.initState();
  }


  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(oldWidget) {
    
    super.didUpdateWidget(oldWidget);
  }

  @override
  void afterFirstLayout(BuildContext context){
   
    final IW = MainStateContainer.of(context);
    final me = IW.me;
    
    // print(booksList[0].id);
  }

Future<Null> _refresh() async {
    
  await new Future.delayed(const Duration(milliseconds:5));
  // getLibraryList(null,null);
  return null;
}

  

 @override
  Widget build(BuildContext context) {
    
    final iw = MainStateContainer.of(context);
    final pageWidth = MediaQuery.of(context).size.width;
    final locale = iw.locale;
    final host = iw.hostName;
    

    return  LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) { 
        return Padding(
          padding:EdgeInsets.only(top:0.0),
          child:Material(
            color:(Theme.of(context).primaryColor==Colors.white) ? Colors.black12 : Colors.white12,
            child:SingleChildScrollView(
              child:Center(
                child:ConstrainedBox(
                  constraints: constraints.copyWith(
                    minHeight: constraints.maxHeight,
                    maxHeight: double.infinity,
                  ),
                  child: IntrinsicHeight(
                    child:RefreshIndicator(
                      key: _refreshIndicatorKey,
                      onRefresh: _refresh,
                      child:Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        mainAxisSize: MainAxisSize.max,
                        verticalDirection: VerticalDirection.down,
                        children: <Widget>[
                          Container(
                            height: 180.0,
                            child: new Card(
                              shape : new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(20.0)),
                              child:Material(
                                shape : new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(20.0)),
                                color:Theme.of(context).primaryColor==Colors.white ? Colors.yellow[100] : Colors.black26, 
                                child: new Center(
                                  child:Column(
                                    children: <Widget>[ 
                                      Padding(  
                                        padding: EdgeInsets.only(left: 20.0,right:20.0,top:10.0,bottom:40.0),
                                        child:Text(DemoLocalizations.of(context).trans(this.name.split(" ").join(""))["MainBox1"],
                                            maxLines: 3,
                                            style:Theme.of(context).textTheme.body2,
                                            
                                          ),
                                        ),

                                        Row(
                                          
                                          verticalDirection: VerticalDirection.down,
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          mainAxisSize: MainAxisSize.max,
                                          children:[
                                            IranpharmaButton(
                                              height: 50.0,
                                              width:100.0,
                                              radius:20.0,
                                              color:Colors.deepOrange,
                                              onPressed:(){
                                                  this.tabController.animateTo(2);
                                              },
                                              child:Text("Create",style:TextStyle(color:Colors.white))
                                            ),
                                            IranpharmaButton(
                                              height: 50.0,
                                              width:100.0,
                                              radius:20.0,
                                              color:Colors.yellow,
                                              onPressed:(){
                                                  this.tabController.animateTo(1);
                                              },
                                              child:Text("Join",style:TextStyle(color:Colors.black))
                                            ),
                                          ]
                                        ),
                                      ]
                                    )
                                  ),
                                ),
                              ),
                              decoration: new BoxDecoration(
                                boxShadow: [
                                  new BoxShadow(
                                    color: Colors.black,
                                    blurRadius: 20.0,
                                  ),
                                ]
                              ),
                            ),
                          Container(
                            height: 180.0,
                            child: new Card(
                              shape : new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(20.0)),
                              child:Material(
                                shape : new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(20.0)),
                                color: Theme.of(context).primaryColor==Colors.white ? Colors.lightBlue[100] : Colors.black26, 
                                child:new Center(
                                  child:Column(
                                    children: <Widget>[ 
                                      Padding(  
                                        padding: EdgeInsets.only(left: 20.0,right:20.0,top:10.0,bottom:40.0),
                                        child:Text(DemoLocalizations.of(context).trans(this.name.split(" ").join(""))["MainBox2"],
                                            maxLines: 3,
                                            style:Theme.of(context).textTheme.body2,
                                          ),
                                        ),
                                        Row(
                                          verticalDirection: VerticalDirection.down,
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          mainAxisSize: MainAxisSize.max,
                                          children:[
                                            IranpharmaButton(
                                              height: 50.0,
                                              width:100.0,
                                              radius:20.0,
                                              color:Colors.green,
                                              onPressed:(){
                                                  this.tabController.animateTo(4);
                                              },
                                              child:Text("Details",style:TextStyle(color:Colors.black))
                                            ),
                                            IranpharmaButton(
                                              height: 50.0,
                                              width:100.0,
                                              radius:20.0,
                                              color:Colors.deepOrange,
                                              onPressed:(){
                                                  this.tabController.animateTo(3);
                                              },
                                              child:Text("Watch",style:TextStyle(color:Colors.black))
                                            ),
                                          ]
                                        ),
                                      ]
                                    )
                                  ),
                                ),
                              ),
                              decoration: new BoxDecoration(boxShadow: [
                                new BoxShadow(
                                  color: Colors.black,
                                  blurRadius: 20.0,
                                ),
                              ]
                            ),
                          ),
                            // Container(
                            //   height: 50.0,
                            //   child: Align(
                            //     alignment: Alignment.bottomCenter,
                            //     child: Container(
                            //       width: double.infinity,
                            //       color: Colors.black,
                            //       padding: EdgeInsets.only(top:50.0,bottom:50.0),
                            //       child: Text('FOOTER', textAlign: TextAlign.center,),
                            //     ),
                            //   ),
                            // ),
                          ],
                        ),
                      
                    )
                  ) 
                )
            
              ),
            ),
          )
        );
      }
    );  
  }



  // @override
  // TODO: implement wantKeepAlive
  // bool get wantKeepAlive => true;


}
