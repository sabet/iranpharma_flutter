import 'package:after_layout/after_layout.dart';
import 'package:iranpharma/home_page.dart';
import 'package:iranpharma/main_state_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:simple_animations/simple_animations/controlled_animation.dart';
import 'package:simple_animations/simple_animations/multi_track_tween.dart';
import 'animated_background.dart';
import 'blocs/router_bloc.dart';
import 'blocs/theme_bloc.dart';

class AnimatedListTile extends StatefulWidget {
  final ThemeBloc themeBloc;
  final ThemeData themeData;
  final RouterBloc routerBloc;
  final bool iconOnly, isMobile;
  final String title;
  
  AnimatedListTile({Key key, this.themeBloc, this.themeData, this.routerBloc,this.iconOnly, this.isMobile, this.title})
      : super(key: key);


  @override
  _AnimatedListTile createState() =>
      new _AnimatedListTile(this.themeBloc, this.routerBloc, this.themeData,this.iconOnly, this.isMobile, this.title,);

  void main() async {
    // See https://github.com/flutter/flutter/wiki/Desktop-shells#target-platform-override

// We use the database factory to open the database

//          await store.record('settings').put(db, {'offline': true});
  }
}
class _AnimatedListTile extends State<AnimatedListTile> with AfterLayoutMixin<AnimatedListTile>{
  final ThemeBloc themeBloc;
  final ThemeData themeData;
  final RouterBloc routerBloc;
  bool iconOnly, isMobile;
  final String title;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();


  _AnimatedListTile(this.themeBloc, this.routerBloc, this.themeData,
      this.iconOnly, this.isMobile, this.title);


  @override
  void initState() {
//    routerBloc.routeObservable.listen((data){

//      print("theme route success : " + data);
//    }, onDone: () {
//      print("Task Done");
//    }, onError: (error) {
//      print("Some Error " + error);
//    });


    super.initState();
  }


  @override
  void didUpdateWidget(oldWidget) {
    print("updtate widget theme selector :");
    super.didUpdateWidget(oldWidget);
  }


//  _HomePageState createState() => new _HomePageState();



  @override
  Widget build(BuildContext context) {
    
    
    final myInheritedWidget = MainStateContainer.of(context);
    final theme = myInheritedWidget.sharedTheme;
    final sideNav = myInheritedWidget.sideNav;



    var destRoute = "/" + title;
    var icon = Icons.home;
    switch (title) {
      case "Login":
        icon = Icons.keyboard;
        break;
      case "Register":
        icon = Icons.keyboard;
        break;
      case "Settings":
        icon = Icons.settings;
        break;
    }
    return StreamBuilder(
        stream: routerBloc.routeObservable,
        initialData: ((routerBloc.get() != null) && (routerBloc.get() != "/switchLang")) ? routerBloc.get() : "/",
        builder: (BuildContext context, snapshot) {
          if (snapshot != null && snapshot.hasData) { // route
            var selected = (snapshot.data.toString() == ("/" + title));           
              return Stack(
                children: <Widget>[
                  selected ? Positioned.fill(
                  child:ControlledAnimation(
                        playback: Playback.PLAY_FORWARD,
                        tween: (theme.primaryColor==Colors.white) ? sideNav.tween : sideNav.tween,
                        duration:sideNav.tween.duration,
                        builder: (context, animation) {
                          return Container(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: [
                                      animation["color1"],
                                      animation["color2"],
                                      animation["color3"],
                                    ]
                                )
                            ),
                          );
                        },
                    ),
                  ):Container(),
                    ListTile(
                      leading: SizedBox(
                        width: 40.0,
                        height: 40.0,
                        child: Icon(
                            icon, color: selected
                            ? Colors.black
                            : Colors
                            .white,
                            size: 36.0),
                      ),
                      contentPadding: EdgeInsets.symmetric(
                          vertical: 1.0, horizontal: 8.5),
                      title: (isMobile || sideNav.open) ? Text(
                          (title != "" ? title : "Home"), maxLines: 1,
                          style: TextStyle(
                              color: selected ? (theme.primaryColor==Colors.white) ? Colors.black : Colors.white : (theme.primaryColor==Colors.white) ? theme.accentColor : Colors.white,
                              fontSize: 18.0)) : Container(),
                      selected: selected,
                      onTap: () {
                        if (isMobile) {
                          Navigator.of(context).pop();
                        }
                        routerBloc.route(destRoute);
                        // ...
                      },
                    ),

                                 selected ? Positioned.fill(
                             child:AnimatedWave(
                               height: 50.0,
                               speed: 1.0,
                             ),
                           ):Container()
                  ]
              );
            
    
          }
          else {
            return Container();
          }
        }
    );
  }

  Widget animatedBackground(context, theme) {

  }

  @override
  void afterFirstLayout(BuildContext context) {
    // Calling the same function "after layout" to resolve the issue.
//    observeThemeBloc(context);
  }

}