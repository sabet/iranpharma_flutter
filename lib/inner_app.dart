
import 'package:iranpharma/localization/localization_delegate.dart';
import 'package:iranpharma/main_state_container.dart';
import 'package:iranpharma/pages/login.dart';
import 'package:iranpharma/pages/register.dart';
import 'package:iranpharma/server_page.dart';
import 'package:iranpharma/settings_page.dart';
import 'package:iranpharma/widgets/page_item.dart';
import 'package:flutter/material.dart';
import 'package:iranpharma/blocs/theme_bloc.dart';
import 'package:iranpharma/blocs/router_bloc.dart';
import 'package:iranpharma/home_tabs_page.dart';
import 'package:page_transition/page_transition.dart';
import 'package:flutter\_localizations/flutter\_localizations.dart';
import 'blocs/authentication_bloc.dart';


void main(){
  
}
class InnerApp extends StatelessWidget {
  final ThemeBloc themeBloc;
  final RouterBloc routerBloc;
  final GlobalKey<ScaffoldState> mainScaffoldKey;
  final RouterBloc chatRouterBloc;
  final ThemeData themeData;
  
  final AuthenticationBloc authenticationBloc;
  final TabController tabController;
  final DemoLocalizationsDelegate localizationDelegate;
  final Locale locale = Locale('fa',"FA");
  InnerApp({this.themeBloc,this.routerBloc, this.themeData, this.authenticationBloc,this.localizationDelegate, this.chatRouterBloc, this.mainScaffoldKey, this.tabController});
  
  
  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> appScaffoldKey = new GlobalKey<ScaffoldState>();
    final homeTabsPage = HomeTabsWidget(key: UniqueKey(),themeBloc: themeBloc,tabController: tabController ,routerBloc: routerBloc,themeData: themeData,chatRouterBloc:chatRouterBloc,mainScaffoldKey: mainScaffoldKey,appScaffoldKey:appScaffoldKey,authenticationBloc:authenticationBloc,localizationsDelegate:localizationDelegate);
    final settingsPage = SettingsPage(key:UniqueKey(),themeBloc: themeBloc, routerBloc: routerBloc,themeData:themeData,localizationDelegate:localizationDelegate);
    final serverPage = ServerPage(routerBloc:routerBloc,);
    final loginPage = LoginPage(key:UniqueKey(),themeBloc: themeBloc, routerBloc: routerBloc,themeData:themeData,authenticationBloc: authenticationBloc);
    final registerPage = RegisterPage(key:UniqueKey(),themeBloc: themeBloc, routerBloc: routerBloc,themeData:themeData,authenticationBloc: authenticationBloc);
    

    
    final inheritedWidget = MainStateContainer.of(context);
   
    // final localization = inheritedWidget.localizationDelegate;
    print("routed : " + routerBloc.get().toString() );
    return MaterialApp(
      supportedLocales: [  
            const Locale('en', 'EN'),
            const Locale('fa', 'FA'), 
            const Locale('es',"ES"),
        ],  
      localizationsDelegates: [  
            inheritedWidget.localizationDelegate,  
            GlobalMaterialLocalizations.delegate,  
            GlobalWidgetsLocalizations.delegate  
      ], 
      locale: inheritedWidget.locale, 
      // localeResolutionCallback: (Locale locale, Iterable<Locale> supportedLocales) {  
      //   for (Locale supportedLocale in supportedLocales) {  
      //     if (supportedLocale.languageCode == locale.languageCode || supportedLocale.countryCode == locale.countryCode) {  
      //         return supportedLocale;  
      //     }  
      //   }  
      //   return supportedLocales.first;  
      // },  
      theme: Theme.of(context),
      initialRoute: "/",
      debugShowCheckedModeBanner: false,
//    home:HomeTabsWidget(themeBloc: themeBloc, routerBloc: routerBloc,parentContext: context,),
      onGenerateRoute: (RouteSettings routeSettings){
        switch (routeSettings.name){
          case "/" : return BaseMaterialPageRoute(builder: (context)=> homeTabsPage);break;
          // case "/Settings" : return MaterialPageRoute( builder: (context)=> settingsPage);break;
          // case "/Server" : return MaterialPageRoute( builder: (context)=> serverPage);break;
          case "/Login" : return PageTransition(child: loginPage , type: PageTransitionType.downToUp,duration: Duration(milliseconds:250),curve: Curves.easeInOut);break;
          case "/Register" : return PageTransition(child: registerPage, type: PageTransitionType.downToUp,duration: Duration(milliseconds:250),curve: Curves.easeInOut);break;
          // case 
        }
        if(routeSettings.name.startsWith("/medicine")){

          final gameId = int.parse(routeSettings.name.split("/medicine/")[1]);
          var title = "";
          


          return MaterialPageRoute(
            builder: (context) {
              return PageItem(num: gameId,name:title,routerBloc: routerBloc,);
            },
            fullscreenDialog: true,
          );
        }
      },
    );
  }

  
  
}

// NEW NEW NEW NEW NEW
// NEW NEW NEW NEW NEW
class BaseMaterialPageRoute<T> extends MaterialPageRoute<T> {
  Widget result;

  BaseMaterialPageRoute({
    @required WidgetBuilder builder,
    RouteSettings settings,
    bool maintainState = true,
    bool fullscreenDialog = false,
  })  : assert(builder != null),
        assert(maintainState != null),
        assert(fullscreenDialog != null),
        assert(opaque),
        super(
            builder: builder,
            settings: settings,
            maintainState: maintainState,
            fullscreenDialog: fullscreenDialog);

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    if (result == null) {
      result = builder(context);
    }
    assert(() {
      if (result == null) {
        throw FlutterError(
            'The builder for route "${settings.name}" returned null.\n'
            'Route builders must never return null.');
      }
      return true;
    }());
    return Semantics(
      scopesRoute: true,
      explicitChildNodes: true,
      child: result,
    );
  }
}