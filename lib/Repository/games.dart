import 'dart:convert';
import 'package:iranpharma/Repository/api.dart';


class Games {
  String hostName="http://116.203.231.112/api";
  Games();



  
  Future<List<dynamic>> getLibraryList(hostName) async{
    try{
      var res = await Api.get(hostName+"/library");
  //    var jsonResponse = jsonDecode(res.body.toString());
      if(res!=null){
        if (res.statusCode == 200) {
          var jsonResponse = json.decode(res.body);
            return jsonResponse;
          } else {
            print("Request failed with status: ${res.statusCode}.");
            return [{"status":res.statusCode,"message":res.reasonPhrase}];
          }
      }
      else{
        return [{"message":"Connection Error"}];
      }
    }
    catch(exception){

      return [{"error":exception}];
    }
  }
  Future<String> createGame(hstName,body) async{
    try{
      
      var res = await Api.post(hstName+"/api/v1/games/create/",body);
      if(res!=null){
        if(res.body is String){
            return res.body.toString();
        }
        else{
          var jsonResponse = json.decode(res.body);
          return "ERR :"+ jsonResponse["message"];
        }
      }
      else{
        return "";
      }
    }
    catch(e){
      return "ERR :"+ e.toString();
    }
      
// Add in username and password to requestvar url = 'http://example.com/whatsit/create';
  }
  Future<Map> getWaitingGames(hostName,gameName) async{
    var res = await Api.get(hostName+"/api/v1/games/waiting"+"?name="+gameName);
    if(res!=null){
      var jsonResponse = json.decode(res.body);

      if (res.statusCode == 200) {
        print("");
        return jsonResponse;
      } else {
        print("Request failed with status: ${res.statusCode}.");
        return jsonResponse;
      }
    }
    else{
      return {"error":"Eonnection Error"};
    }
  }
  Future<Map> getGameDetails(hostName,gameId) async{
    var res = await Api.get(hostName+"/api/v1/games/"+gameId);
    if(res!=null){
      var jsonResponse = json.decode(res.body);

      if (res.statusCode == 200) {
        print("key : $jsonResponse['username'].");
        return jsonResponse;
      } else {
        print("Request failed with status: ${res.statusCode}.");
        return jsonResponse;
      }
    }
    else{
      return {"error":"Eonnection Error"};
    }
  }

}