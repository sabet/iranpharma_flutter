import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class Api {
  
  static Map<String, String> headers = {};
  static int tokenExpire;

  static Future get(String url) async {
    try{
      
      final http.Response response = await http.get(url, headers: headers).catchError((err){
        print(err);
        err.message="Conntection Error";
        return err;
      }).timeout(Duration(seconds: 4));
      return response;
      
    }
    catch(exception){
      print(exception);
        // exception.message="Conntection Error";
        exception._arguments = "Conntection Error";
        return exception;
    }
    // updateCookie(response);
    
  }

  static Future<Response> post(String url, dynamic data) async {
    var response;
    try{
      response = await http.post(url, body: data, headers: headers).timeout(Duration(seconds: 4));
      if((response.body is String))
        updateCookie(response);
      return response;
    }
    catch(exception){
      print(exception);
      return null;
    }
    
  }

  static void updateCookie(http.Response response,) {
    if(response.statusCode==200){
      if(response.body!="true"){
        String token = json.decode(response.body)['access_token'];
        int expire = json.decode(response.body)['expires_in'];
        if(token!=null){
          setCookie(token);
          tokenExpire = (DateTime.now().millisecondsSinceEpoch +  expire);
        }
      }
    }
    else{
      print("POST : " + response.statusCode.toString());
    }
  }
  static void setCookie(String token) {
    headers['cookie'] = "X-ACCESS-TOKEN="+token.split("@__@")[0];
    headers['Authorization'] = "Bearer "+token.split("@__@")[0];
    print(headers['cookie']);
  }
  static void removeCookies() {
      headers['Authorization'] = null;
      tokenExpire = 0;
      headers['cookie'] =null;
  }
}
