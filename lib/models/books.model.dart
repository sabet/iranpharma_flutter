class BookModel{
  int id;
  int categoryId;
  int price;
  String model;
  String title;
  String color;
  String createdAt;
  String updatedAt;
  String icon;
  String iconLibrary;
  String jDateExpireAt;
  dynamic pivot;



  BookModel({ 
    this.id,
    this.model,
    this.color,
    this.pivot,
    this.createdAt,
    this.updatedAt,
    this.icon,
    this.iconLibrary,
    this.price,
    this.categoryId,
    this.title,
    this.jDateExpireAt,
  });
}