class User{
  int id;
  String title;
  String email;
  String firstname;
  String lastname;
  String phone;
  String role;
  int wallet;
  String createdAt;
  String updatedAt;
  List favoriteMedicines;

  User({ this. id,
  this. title,
  this. firstname,
  this.lastname,
  this.role,
  this.favoriteMedicines,
  this.wallet,
  this. email,
  this. phone,
  this. createdAt,
  this. updatedAt,});
}