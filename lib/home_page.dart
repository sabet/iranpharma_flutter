import 'dart:math';
import 'dart:ui';
import 'package:after_layout/after_layout.dart';
import 'package:iranpharma/Repository/accounts.dart';
import 'package:iranpharma/Repository/api.dart';

import 'package:iranpharma/inner_app.dart';
import 'package:iranpharma/localization/localization.dart';
import 'package:iranpharma/localization/localization_delegate.dart';
import 'package:iranpharma/main_state_container.dart';
import 'package:iranpharma/theme_switch_widget.dart';
import 'package:flutter/material.dart';
import 'package:iranpharma/blocs/theme_bloc.dart';
import 'package:simple_animations/simple_animations.dart';
import 'animated_list_tile.dart';
import 'blocs/authentication_bloc.dart';
import 'package:iranpharma/models/users.model.dart';
import 'blocs/router_bloc.dart';


class MyCustomRoute<T> extends MaterialPageRoute<T> {
  MyCustomRoute({ WidgetBuilder builder, RouteSettings settings })
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      Widget child) {
    if (settings.isInitialRoute)
      return child;
    // Fades between routes. (If you don't want any animation,
    // just return child.)
    return new FadeTransition(opacity: animation, child: child);
  }
}

//
//class _HomePageState extends State<HomePage> {
//
//
//
//  }
//
//
//}

class HomePage extends StatefulWidget {
  final ThemeBloc themeBloc;
  final ThemeData themeData;
  final RouterBloc routerBloc;
  final AuthenticationBloc authenticationBloc;
  final DemoLocalizationsDelegate localizationDelegate;
  
  const HomePage({Key key, this.themeBloc,this.themeData,this.routerBloc, this.authenticationBloc,this.localizationDelegate}) : super(key: key);



  @override
  _HomePage createState() => new _HomePage(UniqueKey(),this.themeBloc,this.routerBloc,this.themeData,this.authenticationBloc,this.localizationDelegate);
  

  void main() async{

  }

}
class _HomePage extends State<HomePage> with AfterLayoutMixin<HomePage> , SingleTickerProviderStateMixin{
  final ThemeBloc themeBloc;
  final RouterBloc routerBloc;
  final ThemeData themeData;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final AuthenticationBloc authenticationBloc;
  final DemoLocalizationsDelegate localizationDelegate;
  var themeObserver;
  final Account accountRepository = Account();
  final RouterBloc chatRouterBloc = RouterBloc(path: "/");
  TabController _tabController;
  String appTitle;
  bool firstLoad;

  _HomePage(Key key,this.themeBloc,this.routerBloc,this.themeData, this.authenticationBloc,this.localizationDelegate);



  @override
  void initState() {
    _tabController = TabController(vsync: this, length: 3,initialIndex: 1);
    super.initState();
  }

 

  @override
  void didUpdateWidget(oldWidget) {
    super.didUpdateWidget(oldWidget);
    
  }

  @override
  void didChangeDependencies(){
    super.didChangeDependencies();
  }

    @override
    void dispose() { 
      themeObserver.cancel();
      super.dispose();
    }

  
  @override
  Widget build(BuildContext context) {
    
    
    // authenticationBloc.updateToken(token);
    final inherittedWidget = MainStateContainer.of(context);
    final me = inherittedWidget.me;
    final pageWidth = MediaQuery.of(context).size.width;
    
    // return DefaultTabController(
    //   length: 3,
    //   initialIndex: 1,
      // child: 
      return Scaffold(
        resizeToAvoidBottomInset: false,
        key: _scaffoldKey,
        backgroundColor: Theme
            .of(context)
            .scaffoldBackgroundColor,
        // drawer: /*getDrawerWidget(context)*/ (pageWidth< 1400.0) ? DrawerWidget(routerBloc: routerBloc,themeBloc:themeBloc,scaffoldKey:_scaffoldKey,sideNavBloc:sideNavBloc,themeData: Theme.of(context),parentContext: context,navigationMenu: getNavigationMenu,authenticationBloc:authenticationBloc):Container(),
        // endDrawer: getEndDrawerWidget(context),
        body: getMaterialBody(context,_scaffoldKey),
      // ),
    );
  }




  Widget getMaterialBody(context,_scaffoldKey){

    final myInheritedWidget = MainStateContainer.of(context);
    final sideNav = myInheritedWidget.sideNav;
    final themeData = myInheritedWidget.sharedTheme;
    final backgroundColor = (Theme.of(context).primaryColor == Colors.white) ? Colors.black : Colors.white30;
    final pageWidth = MediaQuery.of(context).size.width;
    final closedSideWidth = 62.0;
    final openSideWidth = 200.0;
    final openEndside = pageWidth/4.3;
    final closedEndSide = 62.0;
    
      return Material(
        color:( Theme.of(context).primaryColor == Colors.blue ? Colors.white : Colors.black12) ,
        child:  (pageWidth < 1400.0) ? getBigSizeBody(context,_scaffoldKey) : Row( 
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
            Flexible(
              // width: pageWidth - endSideWidth,
              child:Row(
                children: <Widget> [
                  Flexible(
                    child:Padding(
                      padding: EdgeInsets.only(top: 0.0,
                          bottom: 0.0,
                          right: 0.0,
                          left: 0.0
                      ),
                      child: getBigSizeBody(context,_scaffoldKey),
                    ),
                  ),
                ],
              )
            ),
          ],
        ),
      );
    
  }

  Widget getBigSizeBody(context,_scaffoldKey) {
//      print("themeData3"+themeData.toString());
     
      return InnerApp(chatRouterBloc:chatRouterBloc,tabController: _tabController,themeBloc: themeBloc,routerBloc: routerBloc,themeData:themeData,authenticationBloc : authenticationBloc,localizationDelegate:localizationDelegate,mainScaffoldKey:_scaffoldKey);
//    return InnerApp(themeBloc:themeBloc,routerBloc:routerBloc);
  }

  @override
  void afterFirstLayout(BuildContext context) {
    // Calling the same function "after layout" to resolve the issue.
    
    // if(routerBloc.get()==null) {
    //   routerBloc.route("/");
    // }
    var inheritedWidget = MainStateContainer.of(context);
    inheritedWidget.setScaffoldKey(_scaffoldKey);

    final inherittedWidget = MainStateContainer.of(context);
    final sideNav = inherittedWidget.sideNav;
    if((MediaQuery.of(context).size.width < 1400.0) && !sideNav.openChat){
      inherittedWidget.switchChat();
    }
    else if((MediaQuery.of(context).size.width >= 1400.0) && !sideNav.openChat){
      inherittedWidget.switchChat();
    }
    else{
      
    }


    observeThemeBloc(context);
  }

  void observeThemeBloc(BuildContext context){
    final InheritedWidget = MainStateContainer.of(context);
    themeObserver = themeBloc.themeDataStream.listen((data){
     InheritedWidget.setTheme(data);
   }, onDone: () {
     print("Task Done");
   }, onError: (error) {
     print("Some Error " + error);
   });
//    routerBloc.routeObservable.doOnCancel(onCancel);
  }

  void route(context,lastPath,newPath) {
    if (newPath == "/") {
        
        
        Future.delayed(new Duration(milliseconds: 5), () {
          routerBloc.route(newPath);  
        });
    }
    else {
      
      Future.delayed(new Duration(milliseconds: 5), () {
        routerBloc.route(newPath);
      });
    }
  }
}





class AnimatedWave extends StatelessWidget {
  final double height;
  final double speed;
  final double offset;

  AnimatedWave({this.height, this.speed, this.offset = 0.0});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return Container(
        height: height,
        width: constraints.biggest.width,
        child: ControlledAnimation(
            playback: Playback.LOOP,
            duration: Duration(milliseconds: (5000 / speed).round()),
            tween: Tween(begin: 0.0, end: 2 * pi),
            builder: (context, value) {
              return CustomPaint(
                foregroundPainter: CurvePainter(value + offset),
              );
            }),
      );
    });
  }
}

class CurvePainter extends CustomPainter {
  final double value;

  CurvePainter(this.value);

  @override
  void paint(Canvas canvas, Size size) {
    final white = Paint()..color = Colors.white.withAlpha(60);
    final path = Path();

    final y1 = sin(value);
    final y2 = sin(value + pi / 2);
    final y3 = sin(value + pi);

    final startPointY = size.height * (0.5 + 0.4 * y1);
    final controlPointY = size.height * (0.5 + 0.4 * y2);
    final endPointY = size.height * (0.5 + 0.4 * y3);

    path.moveTo(size.width * 0, startPointY);
    path.quadraticBezierTo(
        size.width * 0.5, controlPointY, size.width, endPointY);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.close();
    canvas.drawPath(path, white);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}



