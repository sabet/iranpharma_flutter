
import 'dart:math';
import 'dart:ui';

// import 'package:flare_flutter/flare.dart';
// import 'package:flare_dart/math/mat2d.dart';
// import 'package:flare_dart/math/vec2d.dart';
// import 'package:flare_flutter/flare_controls.dart';


class SwitchController {//extends FlareControls {
  // Store a reference to our face control node (the "ctrl_look" node in Flare)
  // ActorNode _faceControl;
  
  
  // Storage for our matrix to get global Flutter coordinates into Flare world coordinates.
  // Mat2D _globalToFlareWorld = Mat2D();

  // // Caret in Flutter global coordinates.
  // Vec2D _caretGlobal = Vec2D();

  // // Caret in Flare world coordinates.
  // Vec2D _caretWorld = Vec2D();

  // // Store the origin in both world and local transform spaces.
  // Vec2D _faceOrigin = Vec2D();
  // Vec2D _faceOriginLocal = Vec2D();

  // bool _hasFocus = false;

  // Project gaze forward by this many pixels.
  // static const double _projectGaze = 60.0;

  bool value;

  SwitchController();

  // @override
  // bool advance(FlutterActorArtboard artboard, double elapsed) {
  //   super.advance(artboard, elapsed);
    
    
  //   return true;
  // }

  // // Fetch references for the `ctrl_face` node and store a copy of its original translation.
  // @override
  // void initialize(FlutterActorArtboard artboard) {
  //   super.initialize(artboard);
  //   // _faceControl = artboard.getNode("ctrl_face");
  //   // if (_faceControl != null) {
  //   //   _faceControl.getWorldTranslation(_faceOrigin);
  //   //   Vec2D.copy(_faceOriginLocal, _faceControl.translation);
  //   // }
  //   if(!off){
  //     play("day_idle");
  //   }
  //   else{
  //     play("night_idle");
  //   }
    
  // }

 
  // Called by [FlareActor] when the view transform changes.
  // Updates the matrix that transforms Global-Flutter-coordinates into Flare-World-coordinates.
  // @override
  // void setViewTransform(Mat2D viewTransform) {
    
  // }

  // Transform the [Offset] into a [Vec2D].
  // If no caret is provided, lower the [_hasFocus] flag.
 
 

  bool off = false;
  switchValue() {
    if(!off){
      // play("switch_night");
      off = true;
    }
    else{
      // play("switch_day");
      off = false;
    }  
    Future.delayed(Duration(milliseconds: 400),(){
      if(!off){
        // play("day_idle");
      
      }
      else{
        // play("night_idle");
        
      }   
    });
    
  }
  setValue(val){
    if(val==0){
      off=false;
      // play("day_idle");
      Future.delayed(Duration(milliseconds: 100),(){
        // play("day_idle");
      });
    }
    else{
      off=true;  
      // play("switch_night");
      Future.delayed(Duration(milliseconds: 100),(){
        // play("night_idle");
      });
    }
    
  }

 

}