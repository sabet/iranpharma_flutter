import 'package:simple_animations/simple_animations.dart';

class SideNav{
  bool open;
  bool openChat;
  double pageWidth;
  MultiTrackTween tween;
  String tweenName;

  SideNav({this.open,this.openChat,this.pageWidth,this.tween,this.tweenName});
}