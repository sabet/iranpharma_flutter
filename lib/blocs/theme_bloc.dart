import 'package:iranpharma/blocs/database_bloc.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:async';


class DemoTheme {
  final String name;
  final ThemeData data;
  const DemoTheme(this.name, this.data);
}

class ThemeBloc{

  final Stream<ThemeData> themeDataStream;
  final Sink<DemoTheme> selectedTheme;
  static ThemeData selectedThemeObject;

  void main() async{
    DatabaseBloc databaseBloc = DatabaseBloc();

    var db = await databaseBloc.getDbInstance();
    var store = databaseBloc.getStoreInstance();
    await store.record('theme').get(db).then((value){



    });
  }
  factory ThemeBloc() {

    final selectedTheme = PublishSubject<DemoTheme>();
    final themeDataStream = selectedTheme
        .distinct()
        .map((theme)=>
          theme.data);
    return ThemeBloc._(themeDataStream, selectedTheme);
  }

  const ThemeBloc._(this.themeDataStream, this.selectedTheme);


  DemoTheme initialTheme(themeName){
    
    if(themeName == null ){
      themeName = 1;
    }
    if(themeName==1) {
      var themeData = DemoTheme(
        'light',
        ThemeData(
          brightness: Brightness.light,
          accentColor: Color.fromRGBO(54, 79, 179, 1),
          primaryColor: Colors.white,
          primarySwatch: Colors.grey,
          scaffoldBackgroundColor: Colors.white,
          appBarTheme: AppBarTheme(
            brightness: Brightness.light,
            iconTheme: IconThemeData(
              color: Color.fromRGBO(54, 79, 179, 1),
              size: 24.0,

            ),
            textTheme: new TextTheme(
              headline: new TextStyle(color: Color.fromRGBO(54, 79, 179, 1)),
              subtitle: new TextStyle(color: Colors.black),
              subhead: new TextStyle(color: Colors.black, fontSize: 12.0),
            ),
          ),
          tabBarTheme: new TabBarTheme(
  //          labelColor : Colors.white,
  //          unselectedLabelColor: Colors.white,
  //          unselectedLabelStyle: TextStyle(
  //              color: Colors.white,
  //          ),
          ),
          bottomAppBarColor: Colors.black,
          primaryColorLight: Colors.lightBlue[100],
          textTheme: new TextTheme(
            headline: new TextStyle(color: Color.fromRGBO(106, 50, 211, 1)),
            subtitle: new TextStyle(color: Colors.black),
            subhead: new TextStyle(color: Colors.white),
          ),
          primaryTextTheme: TextTheme(
              title: TextStyle(
                color: Colors.white,
              )
          ),
        ),
      );

      selectedThemeObject = themeData.data;
      return themeData;
    }
    else if(themeName == 2){
      var themeData = DemoTheme(
        'dark',
        ThemeData(
          brightness: Brightness.dark,
          accentColor: Colors.deepPurpleAccent[300],
          primaryColor: Colors.deepPurple[900],
          bottomAppBarColor: Colors.black,
          primaryColorLight: Colors.indigoAccent,
          //          primarySwatch: Colors.grey,
          scaffoldBackgroundColor: Colors.black38,
          appBarTheme: AppBarTheme(
            brightness: Brightness.dark,
            iconTheme: IconThemeData(
              color: Colors.white70,
              size: 20.0,

            ),
            textTheme: new TextTheme(
              headline: new TextStyle(color: Colors.white),
              subtitle : new TextStyle(color: Colors.white),
              subhead: new TextStyle(color: Colors.black,fontSize: 12.0),
              title: new TextStyle(color: Colors.white,fontSize: 18.0),
            ),
          ),
          tabBarTheme: new TabBarTheme(
//            labelColor : Colors.white,
//            unselectedLabelStyle: TextStyle(
//                color: Colors.white,
//            ),
            unselectedLabelColor: Colors.black12,
          ),
          secondaryHeaderColor: Colors.deepPurple,
          textTheme: new TextTheme(
            headline: new TextStyle(color: Colors.white),
            subtitle : new TextStyle(color: Colors.white),
            subhead: new TextStyle(color: Colors.black),
            title: new TextStyle(color: Colors.white,fontSize: 18.0),
          ),
          primaryTextTheme: TextTheme(
            title: TextStyle(
              color: Colors.white,
            ),
          ),
        ),
      );
      selectedThemeObject = themeData.data;
      return themeData;
    }
  }

  Future<ThemeData> getSelectedTheme() async{
    var themeName = await getThemeName();
    return initialTheme(themeName).data;
  }

  Future<int> getThemeName() async{
    DatabaseBloc databaseBloc = DatabaseBloc();
    var db = await databaseBloc.getDbInstance();
    var store = databaseBloc.getStoreInstance();
    return (await store.record('theme').get(db));
  }

  Future<int> setTheme(number) async{
    DatabaseBloc databaseBloc = DatabaseBloc();
    var db = await databaseBloc.getDbInstance();
    var store = databaseBloc.getStoreInstance();
    await store.record('theme').put(db, number);
    return number;
  }
}
