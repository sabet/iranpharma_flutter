import 'package:rxdart/rxdart.dart';
import 'dart:async';
import 'package:iranpharma/blocs/database_bloc.dart';





class AuthenticationBloc {

  String firstname;
  String lastname;
  String username;
  bool usernameStatus;
  bool emailStatus;
  bool phoneNumberStatus;
  bool passwordStatus;
  bool confPasswordStatus;
  String usernameError;
  String emailError;
  String passwordError;
  String confPasswordError;
  String phoneNumberError;
  String email;
  String password;
  String confPass;
  String token; // access token
  int tokenExpire ;
  String key; // login/reg key
  // double pageHeight;
  BehaviorSubject<String> _subjectToken;
  BehaviorSubject<bool> _usernameStat;
  BehaviorSubject<bool> _emailStat;
  BehaviorSubject<bool> _phoneNumberStat;
  BehaviorSubject<bool> _passwordStat;
  BehaviorSubject<bool> _confPasswordStat;

  BehaviorSubject<String> _usernameError;
  BehaviorSubject<String> _emailError;
  BehaviorSubject<String> _phoneError;
  BehaviorSubject<String> _passwordError;
  BehaviorSubject<String> _confPasswordError;


  AuthenticationBloc(){
    _subjectToken = new BehaviorSubject<String>(); 

    _usernameStat = new BehaviorSubject<bool>();
    _emailStat = new BehaviorSubject<bool>();
    _phoneNumberStat = new BehaviorSubject<bool>();
    _passwordStat = new BehaviorSubject<bool>();
    _confPasswordStat = new BehaviorSubject<bool>();

     _usernameError = new BehaviorSubject<String>();
    _emailError = new BehaviorSubject<String>();
    _phoneError = new BehaviorSubject<String>();
    _passwordError = new BehaviorSubject<String>();
    _confPasswordError = new BehaviorSubject<String>();
    // initializes the subject with element already
  }

  Observable<String> get tokenObservable => _subjectToken.stream;

  Observable<bool> get usernameStatObservable => _usernameStat.stream;
  Observable<bool> get emailStatObservable => _emailStat.stream;
  Observable<bool> get phoneNumberStatObservable => _phoneNumberStat.stream;
  Observable<bool> get passwordStatObservable => _passwordStat.stream;
  Observable<bool> get confPasswordStatObservable => _confPasswordStat.stream;

   Observable<String> get usernameErrorObserver => _usernameError.stream;
    Observable<String> get emailErrorObserver => _emailError.stream;
    Observable<String> get phoneNumberErrorObserver => _phoneError.stream;
    Observable<String> get passwordErrorObserver => _passwordError.stream;
    Observable<String> get confPasswordErrorObserver => _confPasswordError.stream;


  void initialize(){

    username="";
    usernameStatus=null;
    emailStatus=null;
    phoneNumberStatus=null;
    passwordStatus=null;
    confPasswordStatus=null;
    usernameError=null;
    emailError=null;
    passwordError=null;
    confPasswordError=null;
    phoneNumberError=null;
    email="";
    password="";
    token="";
    confPass="";
  }

  Future<String> getTokenFromStorage() async{
    DatabaseBloc databaseBloc = DatabaseBloc();
    var db = await databaseBloc.getDbInstance();
    var store = databaseBloc.getStringStoreInstance();
    return (await store.record('X-ACCESS-TOKEN').get(db));
  }

  Future<String> setTokenOnStorage(String token,expiresAt) async{
    DatabaseBloc databaseBloc = DatabaseBloc();
    var db = await databaseBloc.getDbInstance();
    var store = databaseBloc.getStringStoreInstance();
    await store.record('X-ACCESS-TOKEN').put(db, token+"@__@"+expiresAt.toString());
    return token;
  }
Future<String> removeTokenFromStorage() async{
    DatabaseBloc databaseBloc = DatabaseBloc();
    var db = await databaseBloc.getDbInstance();
    var store = databaseBloc.getStringStoreInstance();
    return await store.record('X-ACCESS-TOKEN').delete(db);
    
  }



  void updateToken(inputToken,expiresAt){
    if(expiresAt==0){
      token = null;
      tokenExpire = expiresAt;
      removeTokenFromStorage();
    }
    else{
      token = inputToken;
      tokenExpire = DateTime.now().millisecondsSinceEpoch + expiresAt;
       setTokenOnStorage(token,tokenExpire).then((response){
        _subjectToken.sink.add(token); // main must listen to this shit and do the getUser and reload the whole shit
      });
    }
   
  
  }
  void updateKey(inputKey){
    key = inputKey;
  }

  String getError(type){
    if(type=="username"){
      return usernameError;
    }
    else if(type=="email"){
      return emailError;
    }
    else if(type=="phoneNumber"){
      return phoneNumberError;
    }
    else if(type=="password"){
      return passwordError;
    }
    else if(type=="confPassword"){
      return confPasswordError;
    }
    else{
      return null;
    }
  }

  void setError(type,err){
    if(type=="username"){
      usernameError = err;
      _usernameError.sink.add(err);
    }
    else if(type=="email"){
      emailError = err;
      _emailError.sink.add(err);
    }
    else if(type=="phoneNumber"){
      phoneNumberError = err;
      _phoneError.sink.add(err);
    }
    else if(type=="password"){
      passwordError = err;
      _passwordError.sink.add(err);
    }
    else if(type=="confPassword"){
      confPasswordError = err;
      _confPasswordError.sink.add(err);
    }
    else{
      
    }
    return;
  }
  void setInputStatus(type,stat){
    if(type=="email"){
      emailStatus = stat;
      _emailStat.sink.add(emailStatus);
    }
    else if(type=="phoneNumber"){
      phoneNumberStatus = stat;
      _phoneNumberStat.sink.add(phoneNumberStatus);
    }
    else if(type=="username"){
      usernameStatus = stat;
      _usernameStat.sink.add(usernameStatus);
    }
    else if(type=="password"){
      passwordStatus = stat;
      _passwordStat.sink.add(passwordStatus);
    }
    else if(type=="confPassword"){
      confPasswordStatus = stat;
      _confPasswordStat.sink.add(confPasswordStatus);
    }
    else{

    }
    return;
  }
  void updateUsername(s){
    username = s;
  }
  void updateFirstname(s){
    firstname = s;
  }
  void uptateLastName(s){
    lastname = s;
  }
  void updatePassword(p){
    password = p;
  }
  void updateConfPass(c){
    confPass = c;
  }
  void updateEmail(e){
    email = e;
  }

  String getPassword(){
    return password;
  }
  String getConfPass(){
    return confPass;
  }
  String getFirstName(){
    return firstname;
  }
  String getLastName(){
    return lastname;
  }
  String getUsername(){
    return username;
  }
  String getEmail(){
    return email;
  }
  String getKey(){
    return key;
  }
  String getToken(){
    return token;
  }





  void dispose(){
    _subjectToken.close();

    _usernameStat.close();
    _passwordStat.close();
    _emailStat.close();
    _phoneNumberStat.close();
    _confPasswordStat.close();

    _usernameError.close();
    _emailError.close();
    _phoneError.close();
    _passwordError.close();
    _confPasswordError.close();
  }


  
}