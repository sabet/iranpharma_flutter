import 'dart:io'; 
import 'package:flutter/widgets.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:async';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';
import 'package:simple_permissions/simple_permissions.dart';



class DatabaseBloc {
  Database db;
  StoreRef store;
  String path;
  StoreRef stringStore;

  DatabaseBloc({this.path}){
    this.store = StoreRef<String,int>.main();
    this.stringStore = StoreRef<String,String>.main();
  }


  Future<Database> getDbInstance() async{
    WidgetsFlutterBinding.ensureInitialized();
    String tempPath="sample.db";
    if(Platform.isIOS || Platform.isAndroid) {
      Directory tempDir;
      tempDir  = await getTemporaryDirectory();

      if(Platform.isAndroid) {
        PermissionStatus writePerm = await SimplePermissions.requestPermission(
            Permission.WriteExternalStorage);
        print("write status : " + writePerm.toString());
        if ((writePerm == PermissionStatus.authorized)) {
          // code of read or write file in external storage (SD card)
          tempPath = tempDir.path + "/temp/sample.db";
        }
        else{
          tempPath = tempDir.path + "/temp/sample.db";
        }
      }
      else {
        if(tempDir!=null){
          tempPath = tempDir.path + "/temp/sample.db";
        }
        else{
          tempPath ="/temp/sample.db";
        }
      }

    }

    // WEB 
    // tempPath ="sample";

    path = tempPath;
    DatabaseFactory dbFactory = databaseFactoryIo;
// We use the database factory to open the database
    var db = await dbFactory.openDatabase(path, version: 1,
        onVersionChanged: (db, oldVersion, newVersion) async {
          // If the db does not exist, create some data
          if (oldVersion == 0) {
//          await store.add(db, {'name': 'Lamp', 'price': 10});
//          await store.add(db, {'name': 'Chair', 'price': 15});
          }
          else{

          }
        });
    return db;
  }
  StoreRef getStoreInstance(){
    return store;
  }
  StoreRef getStringStoreInstance(){
    return stringStore;
  }

  void dispose(){

  }


  
}