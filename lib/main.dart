import 'package:after_layout/after_layout.dart';
import 'package:iranpharma/blocs/authentication_bloc.dart';
import 'package:iranpharma/main_state_container.dart';
import 'package:iranpharma/server_page.dart';
import 'package:iranpharma/side_nav.dart';
import 'package:flutter/material.dart';
import 'package:iranpharma/home_page.dart';
import 'dart:io';
import 'package:iranpharma/blocs/theme_bloc.dart';
import 'package:iranpharma/blocs/router_bloc.dart';
import 'package:iranpharma/blocs/database_bloc.dart';
import 'package:iranpharma/Repository/accounts.dart';
import 'package:iranpharma/models/users.model.dart';
import 'package:iranpharma/Repository/api.dart';
import 'package:sembast/sembast.dart';
import 'package:iranpharma/localization/localization_delegate.dart';
import 'package:simple_animations/simple_animations/multi_track_tween.dart';
import 'package:flutter/foundation.dart'
    show debugDefaultTargetPlatformOverride;



void main() async{
  // See https://github.com/flutter/flutter/wiki/Desktop-shells#target-platform-override
  debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;
  DatabaseBloc databaseBloc = DatabaseBloc();
  final AuthenticationBloc authenticationBloc = AuthenticationBloc();
  final Account accountRepository = Account();
  
// We use the database factory to open the database
  var db = await databaseBloc.getDbInstance();
  var store = databaseBloc.getStoreInstance();

  
  var store2 = databaseBloc.getStringStoreInstance();
  await store.record('theme').get(db).then((value){
      final int themeName = value;
      store2.record('HOST').get(db).then((hostName) async{
        var host = hostName;
        if(hostName==null || hostName.toString().split("").length < 5){
          host = "http://116.203.231.112/api";
        }
        authenticationBloc.getTokenFromStorage().then((token) async{
          DemoLocalizationsDelegate _localizationDelegate = new DemoLocalizationsDelegate();
          if(token!=null && token.toString().split("").length > 10){
          
            authenticationBloc.updateToken(token.split("@__@")[0] , int.parse(token.split("@__@")[1])); // + 
            
            var tk = token.split("@__@")[0];


            final locale = new Locale('fa','FA');
            int expiresAt = int.parse(token.split("@__@")[1]);
            if((DateTime.now().millisecondsSinceEpoch >= expiresAt ) || (tk==null)){
              runApp(ThemeSwitcherApp(db, store, themeName,null,_localizationDelegate,locale,host,tk));
            }
            else{
              Api.setCookie(tk);
              getMe(accountRepository,host+"/profile").then((user){    
                if(user["error"]!=null){
                    user = null;
                }
              
                
                runApp(ThemeSwitcherApp(db, store, themeName,user,_localizationDelegate,locale,host,tk));
              });
            }
          }
          else{
            store2.record('LANG').get(db).then((langCode) async{
              var locale = new Locale('fa','FA');
              if(langCode!=null){
                locale = new Locale(langCode,langCode.toString().toUpperCase());
              }
              runApp(ThemeSwitcherApp(db, store, themeName,null,_localizationDelegate,locale,host,null));
            });
          }
        });
        
      });
      
  });
//          await store.record('settings').put(db, {'offline': true});
}




Future<Map> getMe(Account accountRepository,url) async{
  return (await accountRepository.getMe(url));
}

class ThemeSwitcherApp extends StatelessWidget {
  final StoreRef store;
  final Database db;
  final ThemeBloc themeBloc = ThemeBloc();
  final RouterBloc routerBloc = RouterBloc();
  final  AuthenticationBloc  authenticationBloc = AuthenticationBloc();
  final int themeName;
  final dynamic user;
  final DemoLocalizationsDelegate localizationDelegate;
  final Locale locale;
  final String token;
  final String host;
  final serverPage = ServerPage(host:"http://116.203.231.112/api");
  ThemeSwitcherApp(this.db,this.store,this.themeName,this.user,this.localizationDelegate,this.locale,this.host,this.token);


  @override
  Widget build(BuildContext context) {

    print("realshit selected theme : " + themeName.toString());
    var demoTheme = themeBloc.initialTheme(themeName);
    var themeData =  null;
    if(demoTheme!=null && demoTheme.data !=null) {
      themeData = demoTheme.data;
      print("esme theme an : " + demoTheme.name);
    }
    final tween = MultiTrackTween([
          Track("color1").add(Duration(milliseconds: 500),
              ColorTween(begin: themeData.accentColor, end: themeData.accentColor)),
          Track("color2").add(Duration(milliseconds: 200),
              ColorTween(begin: themeData.primaryColor, end: themeData.accentColor)),
          Track("color3").add(Duration(milliseconds: 500),
              ColorTween(begin: themeData.accentColor, end: themeData.primaryColor))
        ]);
    var me = (user==null) ? null : new User(title: user["title"],
          email: user["email"],
          phone: user["phone"] ?? "",
          wallet: user["waller"],
          role : user["role"],
          createdAt: user["created_at"],
          updatedAt: user["updated_at"],
          firstname: user["firstname"],
          favoriteMedicines: user["favorite_medicines"],
          id : user["id"],
          lastname : user["lastname"]
          );

    
    final sideNav = new SideNav(open: false,openChat: true,pageWidth: 0.0,tween:tween,tweenName: "1");
    final localizationDel = new DemoLocalizationsDelegate();

    // if(this.firstLoad){
    //   localizationDelegate.load(inherittedWidget.locale).then((tempLocalization)=>{
    //     setState((){
    //       appTitle = tempLocalization.trans("AppTitle");
    //       firstLoad = false;
    //     })
    //   });
    // }

    return MainStateContainer(
      // me: user,
      hostName: host,
      authenticationBloc: authenticationBloc,
      me:me,
      mainRouter: routerBloc,
      locale:locale,
      token:token,
      booksList: [],
      connectionsMap: {},
      localizationDelegate:localizationDel,
      sharedTheme: demoTheme.data,
      sideNav: sideNav,
      child:StreamBuilder<ThemeData>(
        initialData: themeData,
        stream: themeBloc.themeDataStream,
        builder: (BuildContext context, AsyncSnapshot<ThemeData> snapshot){
          return MaterialApp(
              // localeResolutionCallback: (Locale locale, Iterable<Locale> supportedLocales) {  
              //       for (Locale supportedLocale in supportedLocales) {  
              //         if (supportedLocale.languageCode == locale.languageCode || supportedLocale.countryCode == locale.countryCode) {  
              //           return supportedLocale;  
              //         }  
              //       }  
              
              //       return supportedLocales.first;  
              // }, 
              title: 'Iran Pharma',
              debugShowCheckedModeBanner:false,
              theme: snapshot.data,
    //          home: new HomePage(
    //            themeBloc:themeBloc,
    //          ),
              initialRoute: "/",
    //    home:HomeTabsWidget(themeBloc: themeBloc, routerBloc: routerBloc,parentContext: context,),
              
              onGenerateRoute: (RouteSettings routeSettings){
                switch (routeSettings.name){
                  case "/" : return BaseMaterialPageRoute(
                      builder: (context)=> HomePage(key:UniqueKey(),themeBloc: themeBloc, routerBloc: routerBloc,themeData:snapshot.data,authenticationBloc: authenticationBloc,localizationDelegate:localizationDelegate)
                  );
                  // case "/Server" : return MaterialPageRoute(builder: (context)=> serverPage);break;
    //              case "/login" : return PageTransition(child: LoginPage(themeBloc: themeBloc, routerBloc: routerBloc,themeData:themeData,), type: PageTransitionType.downToUp,duration: Duration(milliseconds:200),curve: Curves.easeInOut);break;
    //              case "/register" : return PageTransition(child: RegisterPage(themeBloc: themeBloc, routerBloc: routerBloc,themeData:themeData,), type: PageTransitionType.downToUp,duration: Duration(milliseconds:200),curve: Curves.easeInOut);break;
                }
              },
            );
        },
      ),
    );
  }
  static bool notWeb(){
    return !(!Platform.isWindows && !Platform.isAndroid && !Platform.isFuchsia && !Platform.isIOS && !Platform.isMacOS && !Platform.isLinux);

  }
  
}

// NEW NEW NEW NEW NEW
class BaseMaterialPageRoute<T> extends MaterialPageRoute<T> {
  Widget result;

  BaseMaterialPageRoute({
    @required WidgetBuilder builder,
    RouteSettings settings,
    bool maintainState = true,
    bool fullscreenDialog = false,
  })  : assert(builder != null),
        assert(maintainState != null),
        assert(fullscreenDialog != null),
        assert(opaque),
        super(
            builder: builder,
            settings: settings,
            maintainState: maintainState,
            fullscreenDialog: fullscreenDialog);

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    if (result == null) {
      result = builder(context);
    }
    assert(() {
      if (result == null) {
        throw FlutterError(
            'The builder for route "${settings.name}" returned null.\n'
            'Route builders must never return null.');
      }
      return true;
    }());
    return Semantics(
      scopesRoute: true,
      explicitChildNodes: true,
      child: result,
    );
  }
}





