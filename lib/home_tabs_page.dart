import 'dart:io';
import 'package:flutter/gestures.dart';
import 'package:iranpharma/Repository/api.dart';
import 'package:iranpharma/animated_list_tile.dart';
import 'package:iranpharma/animationControllers/tedy_controller.dart';
import 'package:iranpharma/blocs/authentication_bloc.dart';
import 'package:iranpharma/localization/localization.dart';
import 'package:iranpharma/main_state_container.dart';
import 'package:iranpharma/pages/games.dart';
import 'package:iranpharma/pages/profile.dart';
import 'package:iranpharma/pages/home.dart';
import 'package:iranpharma/widgets/inner_app_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:iranpharma/blocs/theme_bloc.dart';
import 'package:iranpharma/blocs/router_bloc.dart';
import 'package:flutter/rendering.dart';
import 'package:after_layout/after_layout.dart';


class HomeTabsWidget extends StatefulWidget {
  final ThemeBloc themeBloc;
  final RouterBloc routerBloc;
  final ThemeData themeData;
  final RouterBloc chatRouterBloc;
  final LocalizationsDelegate localizationsDelegate;
  final AuthenticationBloc authenticationBloc;
  final GlobalKey<ScaffoldState> mainScaffoldKey;
  final GlobalKey<ScaffoldState> appScaffoldKey;
  final RouteObserver<PageRoute> routeObserver;
  final TabController tabController;

  const HomeTabsWidget({Key key,this.themeBloc, this.routerBloc, this.routeObserver, this.themeData, this.chatRouterBloc, this.mainScaffoldKey,this.appScaffoldKey, this.authenticationBloc, this.localizationsDelegate, this.tabController,});

  @override
  _HomeTabsWidget createState() => new _HomeTabsWidget(this.themeBloc,this.routerBloc,this.routeObserver,this.themeData,this.chatRouterBloc,this.mainScaffoldKey,this.appScaffoldKey,this.authenticationBloc,this.localizationsDelegate,this.tabController);
}
class _HomeTabsWidget extends State<HomeTabsWidget> with AfterLayoutMixin<HomeTabsWidget>, SingleTickerProviderStateMixin {
  final ThemeBloc themeBloc;
  final ThemeData themeData;
  final RouterBloc chatRouterBloc;
  final GlobalKey<ScaffoldState> appScaffoldKey;
  final RouterBloc routerBloc;
  final LocalizationsDelegate localizationsDelegate;
  final AuthenticationBloc authenticationBloc;
  final RouteObserver<PageRoute> routeObserver;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var routeObserverObject;
  BuildContext lastContext;
  String appTitle;
  bool firstLoad;
  final TabController tabController;
  // final CounterBloc _counterBloc = new CounterBloc(counter: 0);
  int x = 0;
  final GlobalKey<ScaffoldState> mainScaffoldKey;
  int pageNumber;
  _HomeTabsWidget(this.themeBloc,this.routerBloc,this.routeObserver, this.themeData, this.chatRouterBloc, this.mainScaffoldKey,this.appScaffoldKey, this.authenticationBloc, this.localizationsDelegate, this.tabController);

  


  @override
  initState() {
  //  _teddyController = TeddyController();

    this.firstLoad=true;
    this.appTitle="ایران فارما";
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    routeObserverObject.cancel();
    super.dispose();
  }

  @override
  void didUpdateWidget(oldWidget) {
    print("updtate widget theme selector :");
    super.didUpdateWidget(oldWidget);
  }

  @override
  void afterFirstLayout(BuildContext context) {
    // Calling the same function "after layout" to resolve the issue.
    observeRouterBloc(context);
    setState((){lastContext = context;});
    // tabController.addListener((){
    //       print(tabController.index);
    //       setState((){
    //         pageNumber = tabController.index;
    //         firstLoad=false;
    //       });
    //   });
    
  }

  void observeRouterBloc(BuildContext context){
    routeObserverObject = routerBloc.routeObservable.listen((data){
      if(data!="/switchLang") {
        if((data=="/Login") || (data=="/Register")){
          route(context, data);
        }
        else if(data=="/Reload"){
          // this.reassemble();
          // route(context, data);
        }
        
        // route(context, data);
      }
      else{
       
        setState(() {
            appTitle=DemoLocalizations.of(context).trans("AppTitle");
        });
      }
      print("home tabs route success : " + data);
    }, onDone: () {
      print("Task Done");
    }, onError: (error) {
      print("Some Error " + error);
    });
//    routerBloc.routeObservable.doOnCancel(onCancel);
  }

//  @override
//  void didChangeDependencies(){
//    print("Change dependenncies home tabs : ");
//    super.deactivate();
////    super.didChangeDependencies();
////    dispose();
////    this.dispose();
//  }


  Widget bottomTabBarWidget(context){
    if(firstLoad){
      setState((){
        firstLoad=false;
        
      });
      tabController.addListener((){
            print("index :"+tabController.index.toString());
            setState((){
              pageNumber = tabController.index;

            });
            
        });
    }

    final locale = MainStateContainer.of(context).locale;
    final backgroundColor = (Theme.of(context).primaryColor == Colors.white) ? Colors.black12 : Colors.black12;
    return (MediaQuery.of(context).size.width <= 1200.0) ? PreferredSize(
        preferredSize:Size(60.0, 60.0),
        child:Container(
          color: backgroundColor,
          constraints: BoxConstraints(maxHeight: 60.0),
          height: 60.0,
          child: new Material(
            child:TabBar(
              controller: tabController,
              unselectedLabelColor: Theme.of(context).accentColor,
              indicatorColor: Theme.of(context).accentColor,
              labelColor: Theme.of(context).accentColor,
              // labelStyle: TextStyle(backgroundColor: Colors.black),
              indicatorWeight: 3.0,
              isScrollable: false,
              
              dragStartBehavior:DragStartBehavior.down,
              // indicator: Decoration(chil),
              onTap: (index){
                  setState(() {
                    pageNumber = index;
                  });
              },
              tabs: [
                AnimatedContainer(
                    duration: Duration(milliseconds: 300),
                    curve: Curves.fastOutSlowIn,
                    padding: EdgeInsets.all(0),
                    child:Container(
                      width:400.0,
                      
                      child:
                        Tab(
                          icon: Icon(Icons.person,size:(pageNumber==0) ? 32.0 : 30.0,),
                        )
                    )
                ),
                Tab(
                  icon: AnimatedContainer(
                    duration: Duration(milliseconds: 300),
                    curve:Curves.fastOutSlowIn,
                    child: Image.asset('assets/icons/pill.png',width: (pageNumber==1)?32.0:30.0,height: 32.0,)
                  ),
                  
                ),
                Tab(
                  icon: AnimatedContainer(
                    duration: Duration(milliseconds: 300),
                    curve: Curves.fastOutSlowIn,
                    child:Icon(Icons.home,size:(pageNumber==2) ? 32.0 : 30.0,),)
                ),
              ],
            ),
          ),
        )
    ) : null ;
  }





  @override
  Widget build(BuildContext context) {

    EdgeInsets devicePadding = MediaQuery.of(context).padding;
    final pageWidth = MediaQuery.of(context).size.width;
    final inherittedWidget = MainStateContainer.of(context);
    final locale = inherittedWidget.locale;
    
    return Scaffold(
      
        //appBar : null
      
      //      backgroundColor:( Theme.of(context).primaryColor == Colors.blue ? Colors.white : Colors.black12),
      // appBar: AppBar(
          
      //     automaticallyImplyLeading:false,
      //     title:(locale.countryCode!="fa")?Text(
      //       DemoLocalizations.of(context).trans("AppTitle"),
      //       style: Theme.of(context).appBarTheme.textTheme.headline,
      //     ):Container(),
      //     actions: <Widget>[
      //       IconButton(
      //         icon: Icon(Icons.http),
      //         onPressed: () {
                
      //           // route(context, "/Server");
      //           routerBloc.route("/Server");
      //           // Navigator.of(context).pushNamed("/Server");
      //         },
      //       ),
      //       IconButton(
      //         icon: Icon(Icons.settings),
      //         onPressed: () {
                
      //           // route(context, "/Server");
      //           routerBloc.route("/Settings");
      //           // Navigator.of(context).pushNamed("/Settings");
      //         },
      //       ),
      //       // IconButton(
      //       //   icon: Icon(Icons.notifications),
      //       //   onPressed: () {
                
      //       //   },
      //       // ),
      //       new Stack(
      //         children: <Widget>[
      //           new Container(
      //             padding: EdgeInsets.only(top:20,right:10),
      //             child: (locale.countryCode=="fa") ? Text(
      //               DemoLocalizations.of(context).trans("AppTitle"),
      //               style: Theme.of(context).appBarTheme.textTheme.headline,
      //             ) : Container(), // pick 
      //           ),
      //         ],
      //       ),
      //     ],
      //   ),
      key: appScaffoldKey,
      resizeToAvoidBottomInset:true,
      body: new TabBarView(
        controller:tabController,
        children: [
          ProfilePage(),
          GamesPage(routerBloc: routerBloc,themeBloc:themeBloc,localizationDelegate:localizationsDelegate,themeData:themeData),
          HomePage(),
        ],
      ),
      bottomNavigationBar: bottomTabBarWidget(context),
    );
  }



  Widget teddyWidget(context,devicePadding){

    // var pageHeight = MediaQuery.of(context).size.height;

  }

  Widget topTabBarWidget(context){

    var innerHeaderActions =
          new IconButton(
            icon: new Icon(Icons.keyboard,size:35),
            tooltip: 'shit',
            color: Theme.of(context).accentColor,
            focusColor: Theme.of(context).primaryColorDark,
            onPressed: () => _rightButtonPressed(context),
      );


//    Widget innerHeaderLeading = new IconButton(
//      icon: new Icon(Icons.arrow_back_ios),
//      tooltip: 'Back',
//      onPressed: () => _backButtonPressed(),
//    );
//

    final locale = MainStateContainer.of(context).locale;
    final backgroundColor = Theme
        .of(context)
        .primaryColorLight;

    return (MediaQuery
        .of(context)
        .size
        .width > 1200.0) ? PreferredSize(
      preferredSize: (locale.languageCode=="fa" || locale.languageCode=="ar" ) ?  Size(65.0, 70.0) : Size(65.0, 62.0),
      child: Container(
        constraints: BoxConstraints(maxHeight: 73.0),
        color: backgroundColor,
        child: new Material(
          color: Theme.of(context).backgroundColor,
          child: TabBar(
            labelPadding: EdgeInsets.all(2.0),
            unselectedLabelColor: Theme
                .of(context)
                .textTheme
                .subtitle
                .color,
            indicatorColor: Theme
                .of(context)
                .accentColor,
            labelColor: Theme
                .of(context)
                .accentColor,
            indicatorWeight: 3.0,
            onTap: (index){
              var i = index;
              setState(() {
                pageNumber = i;
              });
              print(pageNumber);
            },
            tabs: [
              Tab(icon: Icon(Icons.local_play,size: 26.0,), text: DemoLocalizations.of(context).trans('GamesHeader'),),
              Tab(icon: Icon(Icons.new_releases,size: 26.0), text: DemoLocalizations.of(context).trans('NewsHeader')),
              Tab(icon: Icon(Icons.people_outline,size: 26.0), text: DemoLocalizations.of(context).trans('SocialHeader')),
              
            ],
          ),
        ),
      ),
    ) :
    PreferredSize(
      preferredSize: Platform.isAndroid ? Size.fromHeight(30.0) :  Size.fromHeight(40.0), // here the desired height
      child: InnerAppBar(
        null,
        innerHeaderActions,
        Icon(Icons.home,size: 30,color:Theme.of(context).accentColor),
        context,
        null
      ),
    );
  }
  void route(context,newPath) {

    if (routerBloc.getLastPath() != null &&
        routerBloc.getLastPath() == newPath) {
          debugPrint("last path : " + routerBloc.getLastPath() + "  ///  path : " + routerBloc.get()  + "  ///  new path : " + newPath);
//          if(newPath=="/"){
//              routerBloc.route("/settings");
//              new Future.delayed(new Duration(milliseconds: 20), () {
//                routerBloc.route(newPath);
//              });
//          }
//          else{
////            if (Navigator.canPop(context)) {
//              routerBloc.route("/");
//              new Future.delayed(new Duration(milliseconds: 20), () {
//                routerBloc.route(newPath);
//              });
////            }
//          }
    }
    else {
      if (newPath != "/") {
        if(newPath=="/Register" || newPath=="/Login"){
          Navigator.pushNamed(appScaffoldKey.currentContext,newPath);
          print("shit2");
        }
        else if (routerBloc.getLastPath() != null) {
          if (x >= 0) {
            if (Navigator.canPop(context)) {
              Navigator.popAndPushNamed(context, newPath);
            }
            else {
              Navigator.pushNamed(context, newPath);
            }
          }
        }
        else {
          Navigator.pushNamed(context, newPath);
        }
      }
      else {
        if (Navigator.canPop(context)) {
          Navigator.pop(context);
        }
      }
    }
  }


  Widget _rightButtonPressed(context){
    return AlertDialog(title: Text("shit",style:Theme.of(context).textTheme.subhead));
  }


}
